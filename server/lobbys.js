const _ = require('lodash')

const data = {
  AAAAAA: {
    id: 'AAAAAA',
    host: '2d23d2f23',
    game: 'schnapsen',
    open: true,
    players: {
      a1: {
        name: 'Admin',
        color: '#f0a',
        ready: false,
        wins: 0,
        losses: 0,
      },
      a2: {
        name: 'Test2',
        color: '#0af',
        ready: false,
        wins: 0,
        losses: 0,
      },
      a3: {
        name: 'Test3',
      },
      a4: {
        name: 'Test4',
      },
      a5: {
        name: 'Test5',
      },
    },
  },
  AAAAAB: {
    id: 'AAAAAB',
    host: 'saj12cds',
    game: 'pan',
    open: true,
    players: {
      b1: {
        name: 'Badmin',
        color: '#0ff',
        ready: true,
        wins: 4,
        losses: 0,
      },
      b2: {
        name: 'Test1',
        color: '#faf',
        ready: false,
        wins: 1,
        losses: 2,
      },
      b3: {
        name: 'Test1',
        color: '#faf',
        ready: false,
        wins: 1,
        losses: 2,
      },
    },
  },
  AAAAAC: {
    id: 'AAAAAC',
    host: 'cdoicbdw',
    game: 'rummy',
    open: true,
    players: {
      c1: {
        name: ' Cadmin',
        color: '#000',
        ready: true,
        wins: 0,
        losses: 4,
      },
    },
  },
  AAAAAD: {
    id: 'AAAAAD',
    host: 'Dadmin',
    game: 'poker',
    open: true,
    players: {
      d1: {
        name: ' Dadmin',
        color: '#000',
        ready: true,
        wins: 0,
        losses: 4,
      },
      d2: {
        name: ' Dadmin2',
        color: '#000',
        ready: true,
        wins: 0,
        losses: 4,
      },
      d3: {
        name: ' Dadmin3',
        color: '#000',
        ready: true,
        wins: 0,
        losses: 4,
      },
      d4: {
        name: ' Dadmin4',
        color: '#000',
        ready: true,
        wins: 0,
        losses: 4,
      },
    },
  },
  AAAAAE: {
    id: 'AAAAAE',
    host: 'Eadmin',
    game: 'war',
    open: true,
    players: {
      e1: {
        name: ' Eadmin',
        color: '#000',
        ready: true,
        wins: 0,
        losses: 4,
      },
      e2: {
        name: ' Eadmin2',
        color: '#000',
        ready: true,
        wins: 0,
        losses: 4,
      },
    },
  },
}

// const createLobby = lobbyID =>
//   _.assign(data, { [lobbyID]: { game: null, players: null } })

const getLobby = (id, key) =>
  _.has(data, id) ? (key ? data[id][key] : data[id]) : null

const getLobbiesWhere = (key, value) => {
  var lobbies = {}
  _.forIn(data, (lobby) => {
    if (lobby[key] === value) {
      _.assign(lobbies, { [lobby.id]: lobby })
    }
  })
  return lobbies
}

const getLobbyPlayerId = (socketID) =>
  _.findKey(data, (lobby) => _.has(lobby.players, socketID))

const createLobby = (lobby) => {
  _.assign(data, {
    [lobby.id]: lobby,
  })
  console.log(`➕ created Lobby ${lobby.id}.`)
}

const playerJoin = (id, socketId, user) => {
  if (_.has(data, id)) {
    _.assign(data[id].players, { [socketId]: user })
    console.log(`🙂 Joined ${id} by ${user.name}`)
  }
}

const updateLobby = (id, key, value) => {
  _.assign(data[id], { [key]: value })
  console.log(`🎲 Updated in ${id} [${key}: ${value}].`)
}

// const updateLobbyGame = (id, game) => {
//   _.assign(data[id], { game: game })
//   console.log(`🎲 Updated in ${id} (${game}).`)
// }

const findLobby = (id) => {
  const hasLobby = _.has(data, id)
  if (hasLobby) {
    console.log(`☑️  Lobby ${id} found.`)
    return id
  } else {
    console.log(`⚠️- Lobby ${id} does not excist.`)
    return false
  }
}

const deleteLobbyPlayer = (id, socketID) => {
  let playerExcist = data[id] && _.has(data[id].players, socketID)
  if (playerExcist) {
    delete data[id].players[socketID]
    console.log(`😰 delete Player ${socketID} from ${id}.`)
  } else console.log(`⚠️- deleting Player failed.`)
}

// const deleteLobby = id => _.omit(data, [`${id}`])
const deleteLobby = (id) => {
  let lobbyExcist = _.has(data, id)
  if (lobbyExcist) {
    delete data[id]
    console.log(`⛔️ delete Lobby ${id}.`)
  } else console.log(`⚠️- deleting Lobby failed.`)
}

module.exports = {
  data,
  getLobby,
  getLobbiesWhere,
  getLobbyPlayerId,
  findLobby,
  createLobby,
  updateLobby,
  playerJoin,
  deleteLobby,
  deleteLobbyPlayer,
}
