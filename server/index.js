const express = require('express')
const socketio = require('socket.io')
const router = require('./router')
const http = require('http')
const https = require('https')

const PORT = process.env.PORT || 5000
const app = express()
const server = http.createServer(app)
const io = socketio(server)

const lobbys = require('./lobbys')

// HELPER FUNCTIONS

const _updateLobbyIfOpen = (id) =>
  lobbys.getLobby(id, 'open') &&
  io.to('join').emit('updateLobbyOpen', lobbys.getLobbiesWhere('id', id))

const _deleteLobbyIfOpen = (id) =>
  lobbys.getLobby(id, 'open') && io.to('join').emit('lobbyDeleteOpen', id)

app.use(router)

io.on('connect', (socket) => {
  console.log(`✔️  Connected (${socket.id}).`)
  socket.emit('established', 'OKAYYY')

  // io.in('join').emit('updateOpenLobbies')

  // JOINPAGE
  socket.on('getLobbies', (callback) => {
    socket.join('join')
    callback(lobbys.getLobbiesWhere('open', true))
  })

  // LOBBY CREATE
  socket.on('createLobby', (lobby) => {
    lobbys.createLobby(lobby)
    socket.join(lobby.id)
    lobbys.playerJoin(lobby.id, socket.id, lobby.players[socket.id])
  })

  // // LOBBY UPDATE GAME
  // socket.on('updateLobbyGame', ({ id, game }) => {
  //   lobbys.updateLobbyGame(id, game)
  //   socket.to(id).emit('updateLobbyGame', game)
  // })

  // LOBBY UPDATE
  socket.on('updateLobbyServer', ({ id, key, value }) => {
    lobbys.updateLobby(id, key, value)
    io.to(id).emit('updateLobbyClient', { key, value })
    _updateLobbyIfOpen(id)
  })

  // LOBBY JOIN
  socket.on('findLobby', (id, callback) => {
    let foundLobby = lobbys.findLobby(id)
    callback(foundLobby)
    // if (foundLobby) {
    //   lobbys.playerJoin(id, socket.id)
    //   socket.leave('join')
    //   socket.join(foundLobby)
    //   io.in(foundLobby).emit('updatePlayers', {
    //     players: lobbys.getLobby(foundLobby, 'players'),
    //   })
    //   io.to(`${socket.id}`).emit('updateLobby', {
    //     key: 'game',
    //     value: lobbys.getLobby(foundLobby, 'game'),
    //   })
    //   io.to(`${socket.id}`).emit('updateLobby', {
    //     key: 'host',
    //     value: lobbys.getLobby(foundLobby, 'host'),
    //   })
    //   _updateLobbyIfOpen(foundLobby)
    // }
  })

  socket.on('joinLobby', ({ id, socketId, user }) => {
    lobbys.playerJoin(id, socketId, user)
    socket.leave('join')
    socket.join(id)
    io.in(id).emit('updatePlayers', {
      players: lobbys.getLobby(id, 'players'),
    })
    // io.to(`${socket.id}`).emit('updateLobby', {
    //   key: 'game',
    //   value: lobbys.getLobby(id, 'game'),
    // })
    // io.to(`${socket.id}`).emit('updateLobby', {
    //   key: 'host',
    //   value: lobbys.getLobby(id, 'host'),
    // })
    io.to(`${socket.id}`).emit('updateLobbyClientAll', lobbys.getLobby(id))
    _updateLobbyIfOpen(id)
  })

  socket.on('deleteLobbyPlayer', ({ id, socketId }) => {
    lobbys.deleteLobbyPlayer(id, socketId)
    socket.leave(id)
    socket.to(id).emit('updatePlayers', {
      players: lobbys.getLobby(id, 'players'),
    })
    _updateLobbyIfOpen(id)
  })

  // LOBBY DELETE
  socket.on('deleteLobby', (id) => {
    _deleteLobbyIfOpen(id)
    socket.leave(id)
    lobbys.deleteLobby(id)
    io.in(id).emit('lobbyDeleted', { id: id })

    // io.in(id).emit('message', 'lobbyDeleted', 'error')
  })

  // SOCKET Disconnect
  socket.on('disconnect', () => {
    console.log(`❌ Disconnected (${socket.id}).`)
    let lobby = lobbys.getLobbyPlayerId(socket.id)
    if (lobby) {
      socket.leave(lobby)
      lobbys.deleteLobbyPlayer(lobby, socket.id)
      io.in(lobby).emit('updatePlayers', {
        players: lobbys.getLobby(lobby, 'players'),
      })
      _updateLobbyIfOpen(lobby)
      if (lobbys.getLobby(lobby, 'host') === socket.id) {
        _deleteLobbyIfOpen(lobby)
        lobbys.deleteLobby(lobby)
        io.in(lobby).emit('lobbyDeleted', { id: lobby })

        // io.in(lobby).emit('message', 'lobbyDeleted', 'error')
      }
    }
  })
})

server.listen(PORT, () => console.log(`🖥  Server running on port ${PORT}.`))
