const express = require('express')
const router = express.Router()
const lobbys = require('./lobbys')

const style = `<style>
  body { background-color: #333; color: grey; margin:0; padding: 0; }
  .container { width: 100vw; height: 100vh; padding: 1rem; }
  h1 { padding: 2rem; text-align: center; color: #00FFAB; }
  b { color: white;}
</style>`

const displayLobbys = () =>
  Object.entries(lobbys.data).map(
    ([key, val]) =>
      `<div><p><b>${key}</b> | game: <b>${val.game}</b> | host: <b>${
        val.host
      }</b></p><ul>${val.players &&
        Object.entries(val.players).map(
          ([key, val]) =>
            `<li>${key} - <span style="color:${val.color};">${val.name}</span></li>`
        )}</ul></div>`
  )

router.get('/', (req, res) => {
  res.send(
    `<head>${style}</head><title>KardGames Server</title><body><div class="container"><h1>KardGames Server is running...</h1>${displayLobbys()}</div>
    <script>console.log(${JSON.stringify(lobbys.data)})</script></body>`
  )
})

module.exports = router
