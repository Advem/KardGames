import { createMuiTheme } from '@material-ui/core/styles'
// import SpartanLightOTF from '../assets/fonts/spartan/SpartanMB-Light.otf'
// import SpartanRegularOTF from '../assets/fonts/spartan/SpartanMB-Regular.otf'
// import SpartanBoldOTF from '../assets/fonts/spartan/SpartanMB-Bold.otf'

// export const SpartanLight = {
//   fontFamily: 'Spartan',
//   fontStyle: 'normal',
//   fontDisplay: 'swap',
//   fontWeight: 300,
//   src: `
//     local('Spartan'),
//     local('Spartan-Light'),
//     url(${SpartanLightOTF}) format('opentype')
//   `,
// }

// export const SpartanRegular = {
//   fontFamily: 'Spartan',
//   fontStyle: 'normal',
//   fontDisplay: 'swap',
//   fontWeight: 400,
//   src: `
//     local('Spartan'),
//     local('Spartan-Regular'),
//     url(${SpartanRegularOTF}) format('opentype')
//   `,
//   unicodeRange:
//     'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
// }

// export const SpartanBold = {
//   fontFamily: 'Spartan',
//   fontStyle: 'normal',
//   fontDisplay: 'swap',
//   fontWeight: 500,
//   src: `
//     local('Spartan'),
//     local('Spartan-Bold'),
//     url(${SpartanBoldOTF}) format('opentype')
//   `,
// }

export const theme = createMuiTheme({
  typography: {
    fontFamily: [
      'Spartan',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        // '@font-face': 'Roboto',
      },
    },
  },
  palette: {
    type: 'dark',
    primary: {
      main: '#00BF80',
      dark: '#00BF80',
      light: '#ff1a66',
    },
    secondary: {
      main: '#ff1a66',
      dark: '#ff1a66',
      light: '#00FFAB',
    },
    background: {
      dark: '#18191A',
      light: '#E4E6EB',
    },
    text: {
      dark: '#E4E6EB',
      light: '#18191A',
    },
    dark: {
      base: '0,0,0',
      reverse: '255,255,255',
    },
    light: {
      base: '255,255,255',
      reverse: '0,0,0',
    },
  },
})
