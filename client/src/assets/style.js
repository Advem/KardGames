export const colors = {
  background: {
    darker: '#18191A',
    dark: '#242526',
    light: '#3A3B3C',
    lighter: '#515354',
  },
  text: {
    title: '#E4E6EB',
    primary: '#D0D2D6',
    secondary: '#A0A3A7',
    subtitle: '#9096A6',
  },
  default: {
    text: '#D0D2D6',
    background: '#515354',
    background2: '#3A3B3C',
  },
  success: {
    text: '#4EFF4E',
    background: '#267326',
    background2: '#1D3C1D',
  },
  warning: {
    text: '#FFE24E',
    background: '#736626',
    background2: '#3C3B1D',
  },
  info: {
    text: '#4ea1ff',
    background: '#264a73',
    background2: '#1c2a3b',
  },
  error: {
    text: '#FF4E4E',
    background: '#732626',
    background2: '#3C1D1D',
  },
  main: {
    primary: '#00BF80',
    secondary: '#ff1a66',
  },
  user: [
    '#EF5350', // 0
    '#EC407A', // 1
    '#AB47BC', // 2
    '#7E57C2', // 3
    '#42A5F5', // 4
    '#26C6DA', // 5
    '#66BB6A', // 6
    '#D4E157', // 7
    '#FFC107', // 8
    '#FF9800', // 9
  ],
}

export const alpha = {
  100: 'FF',
  90: 'E6',
  80: 'CC',
  75: 'BF',
  70: 'B3',
  60: '99',
  50: '80',
  40: '66',
  30: '4D',
  25: '40',
  20: '33',
  10: '1A',
  5: '0D',
  0: '00',
}
export const xs = '@media screen and (max-width: 599px)'
export const ss = '@media screen and (max-width: 360px)'
export const sss = '@media screen and (max-width: 359px)'
export const sm = '@media screen and (min-width: 600px)'
export const md = '@media screen and (min-width: 960px)'
export const lg = '@media screen and (min-width: 1280px)'
export const xl = '@media screen and (min-width: 1920px)'

export const s = {
  xs,
  ss,
  sss,
  sm,
  md,
  lg,
  xl,
}

export const global = {
  containerMargin: {
    [s.xs]: { marginBottom: 2 + 'rem' },
    [s.sm]: { marginBottom: 3 + 'rem' },
    [s.xl]: { marginBottom: 4 + 'rem' },
  },
  lobbyIdButtonBase: {
    marginBottom: '0rem',
    [s.xs]: {
      width: 220,
      fontSize: '1.75rem',
      borderRadius: '2rem',
    },
    [s.sm]: {
      width: 330,
      fontSize: '2.25rem',
      borderRadius: '3rem',
    },
    [s.lg]: { width: 300, fontSize: '2.75rem', borderRadius: '4rem' },
  },
}
