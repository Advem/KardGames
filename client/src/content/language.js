export const text = {
  english: {
    native: 'English',
    home: {
      title: 'Card Games Online',
      subtitle:
        'Playing together is pure fun! Join excisting party or create one and invite your friends',
      createButton: 'CREATE PARTY',
      joinButton: 'JOIN GAME',
    },
    menu: {
      language: 'Language',
      theme: 'Theme',
      dark: 'Dark',
      light: 'Light',
    },
    connection: {
      success: 'Connected to the Server',
      error: 'Disconnected from the Server',
    },
    create: {
      title: 'Create Lobby',
      subtitle:
        "Pick a game. Invite friends by sending them the Lobby ID or select 'Open Lobby' option and wait for other players",
      linkCopied: {
        true: 'Copied link to clipboard!',
        false: 'Tap to copy invitation link',
        error: 'Error! Browser denied access',
      },
    },
    join: {
      title: 'Join Party',
      subtitle:
        'Enter Lobby ID to join a party or play with other gamers waiting for you in Open Lobbies',
      form: {
        placeholder: 'Party ID',
        response: {
          default: '',
          success: 'Connected successfully',
          warning: 'Connecting to Lobby...',
          error: 'Joining failed! The lobby does not excist!',
        },
      },
      ready: 'Ready',
      unready: 'Unready',
    },
    user: {
      subtitle:
        'Enter your nickname that will be displayed during the games and choose favourite color',
      placeholder: 'Type nickname...',
      error: 'Warning! Nickname must be at least 3 characters!',
    },
    openLobbies: {
      title: 'Open Lobbies',
      sort: 'SORT',
      join: 'JOIN',
    },
    games: 'games',
    hosts: 'hosts',
    player: 'player',
    players: 'players',
    newest: 'newest',
  },
  polish: {
    native: 'Polski',
    home: {
      title: 'Gry Karciane Online',
      subtitle:
        'Wspólne granie to czysta frajda! Dołącz do istniejącej grupy lub stwórz własną i zaproś do niej swoich znajomych',
      createButton: 'STWÓRZ POKÓJ',
      joinButton: 'DOŁĄCZ DO GRY',
    },
    menu: {
      language: 'Język',
      theme: 'Motyw',
      dark: 'Ciemny',
      light: 'Jasny',
    },
    connection: {
      success: 'Nawiązano połączenie z serwerem',
      error: 'Utracono połączenie z serwerem',
    },
    create: {
      title: 'Stwórz Pokój',
      subtitle:
        "Wybierz grę. Zaproś znajomych wysyłając im ID Pokoju lub wybierz opcję 'Pokój Otwarty' i poczekaj na innych graczy",
      linkCopied: {
        true: 'Skopiowano link do schowka!',
        false: 'Kliknij, aby skopiować link z zaproszeniem',
        error: 'Błąd! Przeglądarka nie zezwala na dostęp',
      },
    },
    join: {
      title: 'Dołącz do Pokoju',
      subtitle:
        'Wpisz ID Pokoju, aby dołączyć do gry lub dołącz do innych graczy, czekających na Ciebie w Pokojach Otwartych',
      form: {
        placeholder: 'ID Pokoju',
        response: {
          default: '',
          success: 'Pomyślnie połączono',
          warning: 'Łączenie z pokojem...',
          error: 'Błąd podczas dołączania! Pokój nie istnieje',
        },
      },
      ready: 'Gotów',
      unready: 'Niegotów',
    },
    user: {
      subtitle:
        'Wprowadź pseudonim, który będzie wyświetlany podczas gry i wybierz ulubiony kolor',
      placeholder: 'Wpisz pseudonim...',
      error: 'Uwaga! Pseudonim musi mieć conajmniej 3 znaki!',
    },
    openLobbies: {
      title: 'Pokoje Otwarte',
      sort: 'SORTUJ',
      join: 'DOŁĄCZ',
    },
    games: 'gry',
    hosts: 'twórcy',
    player: 'gracz',
    players: 'gracze',
    newest: 'najnowsze',
  },
}
