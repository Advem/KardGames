import { colors as styleColor } from '../assets/style'

import pan from '../assets/games/pan'
import schnapsen from '../assets/games/schnapsen'
import cheat from '../assets/games/cheat'
import war from '../assets/games/war'
import macau from '../assets/games/macau'
import rummy from '../assets/games/rummy'
import poker from '../assets/games/poker'
import bridge from '../assets/games/bridge'
import trifiveeight from '../assets/games/358'
import unknown from '../assets/games/unknown'

export const availableGames = [
  'pan',
  'schnapsen',
  'cheat',
  'war',
  'macau',
  'rummy',
  'poker',
  'bridge',
  '358',
  'test',
]

export const icons = {
  pan,
  schnapsen,
  cheat,
  war,
  macau,
  poker,
  rummy,
  bridge,
  358: trifiveeight,
  unknown,
}

export const colors = {
  pan: styleColor.user[4],
  schnapsen: styleColor.user[6],
  cheat: styleColor.user[1],
  war: styleColor.user[0],
  macau: styleColor.user[8],
  poker: styleColor.user[2],
  bridge: styleColor.user[7],
  rummy: styleColor.user[5],
  358: styleColor.user[3],
  unknown: styleColor.text.primary,
}

export const names = {
  pan: {
    english: 'Pan',
    polish: 'Pan',
  },
  schnapsen: {
    english: 'Schnapsen',
    polish: 'Tysiąc',
  },
  cheat: {
    english: 'Cheat',
    polish: 'Oszukaniec',
  },
  war: {
    english: 'War',
    polish: 'Wojna',
  },
  macau: {
    english: 'Macau',
    polish: 'Makao',
  },
  poker: {
    english: 'Poker',
    polish: 'Poker',
  },
  bridge: {
    english: 'Bridge',
    polish: 'Brydż',
  },
  rummy: {
    english: 'Rummy',
    polish: 'Remik',
  },
  358: {
    english: '3 5 8',
    polish: '3 5 8',
  },
  unknown: {
    english: 'Unknown',
    polish: 'Nieznana',
  },
}
