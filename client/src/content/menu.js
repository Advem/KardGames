import React from 'react'
import { text } from './language'

// Icons
import TranslateIcon from '@material-ui/icons/Translate'
import InvertColorsIcon from '@material-ui/icons/InvertColors'
import InboxIcon from '@material-ui/icons/MoveToInbox'

export const useContent = props => [
  {
    text: 'Inbox',
    icon: <InboxIcon />
  },
  {
    text: 'Mail',
    icon: <InboxIcon />
  },
  {
    text: text[props.language].menu.language,
    icon: <TranslateIcon />,
    children: [
      {
        text: text.english.native,
        state: ['language', 'english'],
        dispatch: { type: 'APP_SET_LANGUAGE', payload: 'english' }
      },
      {
        text: text.polish.native,
        state: ['language', 'polish'],
        dispatch: { type: 'APP_SET_LANGUAGE', payload: 'polish' }
      }
    ]
  },
  {
    text: text[props.language].menu.theme,
    icon: <InvertColorsIcon />,
    children: [
      {
        text: text[props.language].menu.dark,
        state: ['theme', 'dark'],
        dispatch: { type: 'APP_SET_THEME', payload: 'dark' }
      },
      {
        text: text[props.language].menu.light,
        state: ['theme', 'light'],
        dispatch: { type: 'APP_SET_THEME', payload: 'light' }
      }
    ]
  },
  {
    text: 'TEST',
    icon: <InboxIcon />
  }
]
