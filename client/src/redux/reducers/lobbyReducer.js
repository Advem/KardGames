import _ from 'lodash'

const INITIAL_STATE = {
  id: null,
  host: null,
  game: null,
  open: true,
  players: null,
}

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case 'LOBBY_UPDATE_ALL':
      return { ...state, ...payload }
    case 'LOBBY_UPDATE':
      return { ...state, [payload.key]: payload.value }
    // case 'LOBBY_SET_GAME':
    //   return { ...state, game: payload }
    case 'LOBBY_SET_ID':
      return { ...state, id: payload }
    // case 'LOBBY_SET_HOST':
    //   return { ...state, host: payload }
    case 'LOBBY_SET_PLAYERS': {
      _.assign(state, payload)
      return { ...state }
    }
    case 'LOBBY_RESET':
      return INITIAL_STATE

    default:
      return state
  }
}
