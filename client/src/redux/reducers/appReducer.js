const INITIAL_STATE = {
  stage: 'home',
  theme: 'dark',
  language: 'english',
  connected: false,
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'APP_SET_CONNECTED':
      return { ...state, connected: true }
    case 'APP_SET_DISCONNECTED':
      return { ...state, connected: false }
    case 'APP_TOGGLE_CONNECTED': {
      return { ...state, connected: !state.connected }
    }
    // MIGHT USE LATER
    case 'APP_SET_STAGE': {
      return { ...state, stage: action.payload }
    }
    case 'APP_SET_LANGUAGE': {
      return { ...state, language: action.payload }
    }
    // FOR DEVELOPMENT ONLY
    case 'APP_TOGGLE_LANGUAGE': {
      return {
        ...state,
        language: state.language === 'english' ? 'polish' : 'english',
      }
    }
    // GONNA ABANDON THEMING
    case 'APP_SET_THEME': {
      return { ...state, theme: action.payload }
    }
    default:
      return state
  }
}
