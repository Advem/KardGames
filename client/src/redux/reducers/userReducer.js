const INITIAL_STATE = {
  name: null,
  color: null,
  wins: null,
  losses: null,
}

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case 'USER_SET_STATE':
      // return {
      //   ...state,
      //   name: action.payload.name,
      //   color: action.payload.color,
      // }
      return {
        ...state,
        ...payload,
      }
    default:
      return state
  }
}
