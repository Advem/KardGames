export default (state = null, action) => {
  switch (action.type) {
    case 'SOCKET_INIT':
      return action.payload
    default:
      return state
  }
}
