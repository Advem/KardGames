import { combineReducers } from 'redux'
import appReducer from './appReducer'
import socketReducer from './socketReducer'
import lobbiesReducer from './lobbiesReducer'
import lobbyReducer from './lobbyReducer'
import userReducer from './userReducer'

export default combineReducers({
  app: appReducer,
  lobby: lobbyReducer,
  lobbies: lobbiesReducer,
  socket: socketReducer,
  user: userReducer,
})
