import _ from 'lodash'

export default (state = null, action) => {
  switch (action.type) {
    case 'LOBBIES_SET':
      return Object.assign({ ...state }, state, action.payload)
    case 'LOBBIES_UPDATE': {
      return { ...state, [action.payload.id]: action.payload }
    }
    case 'LOBBIES_DELETE': {
      return _.omit(state, action.payload)
    }
    default:
      return state
  }
}
