import io from 'socket.io-client'
import { serverURL as ENDPOINT } from '../utils/endpoints'

export const _connectServer = () => async (dispatch, getState) => {
  let socket = io(ENDPOINT)
  console.log(`📣 Sent connectServer`)
  await dispatch({
    type: 'SOCKET_INIT',
    payload: socket,
  })

  const interval = setInterval(() => {
    let socketConnected = getState().socket.connected
    let stateConnected = getState().app.connected

    if (socketConnected && !stateConnected) {
      console.log('YES')
      dispatch({ type: 'APP_SET_CONNECTED' })
    } else if (!socketConnected && stateConnected) {
      console.log('NO')
      dispatch({ type: 'APP_SET_DISCONNECTED' })
    }
  }, 2000)

  return () => clearInterval(interval)
}

export const _eventsListener = () => async (dispatch, getState) => {
  const { socket } = getState()
  console.log(`📣 Sent established`)
  socket.on('established', (message) => {
    console.log(`👂🏻 Received ${message}`)
  })
  socket.on('updateLobbyClient', ({ key, value }) => {
    dispatch({ type: 'LOBBY_UPDATE', payload: { key, value } })
  })
  socket.on('updateLobbyClientAll', (lobby) => {
    dispatch({ type: 'LOBBY_UPDATE_ALL', payload: lobby })
  })
  socket.on('updatePlayers', (players) =>
    dispatch({ type: 'LOBBY_SET_PLAYERS', payload: players })
  )
  socket.on('updateLobbyOpen', (update) => {
    dispatch({
      type: 'LOBBIES_UPDATE',
      payload: update[Object.keys(update)[0]],
    })
    // console.log(update[Object.keys(update)[0]])
  })
  socket.on('lobbyDeleted', (res) => {
    dispatch({ type: 'LOBBIES_DELETE', payload: res.id })
    dispatch({ type: 'LOBBY_RESET' })
    socket.emit('getLobbies', (callback) => {
      dispatch({ type: 'LOBBIES_SET', payload: callback })
    })
  })
  socket.on('lobbyDeleteOpen', (id) =>
    dispatch({ type: 'LOBBIES_DELETE', payload: id })
  )
  // socket.on('message', (message, variant) => {})
}

// LOCALE
export const _setLobbyId = (id) => async (dispatch) =>
  await dispatch({ type: 'LOBBY_UPDATE', payload: { key: 'id', value: id } })

// LOBBY
export const _setLobbyHost = () => async (dispatch, getState) => {
  let { socket } = getState()
  await dispatch({
    type: 'LOBBY_UPDATE',
    payload: { key: 'host', value: socket.id },
  })
}

export const _updateLobbyL = (key, value) => async (dispatch) =>
  await dispatch({ type: 'LOBBY_UPDATE', payload: { key, value } })

export const _createLobby = () => async (dispatch, getState) => {
  let { socket } = getState()
  let { user } = getState()
  let players = {
    players: { [`${socket.id}`]: { name: user.name, color: user.color } },
  }
  dispatch({ type: 'LOBBY_SET_PLAYERS', payload: players })
  let { lobby } = getState()
  console.log('sending', lobby)
  socket.emit('createLobby', { ...lobby, host: socket.id })
}
export const _updateLobby = (key, value) => async (dispatch, getState) => {
  let { socket } = getState()
  let { lobby } = getState()
  socket.emit('updateLobbyServer', { id: lobby.id, key, value })
}
export const _getLobbies = () => async (dispatch, getState) => {
  let { socket } = getState()
  socket.emit('getLobbies', (callback) => {
    dispatch({ type: 'LOBBIES_SET', payload: callback })
  })
}
export const _deleteLobbyPlayer = () => async (dispatch, getState) => {
  let { socket } = getState()
  let { id } = getState().lobby
  if (id) socket.emit('deleteLobbyPlayer', { id, socketId: socket.id })
}
export const _deleteLobby = (id) => async (dispatch, getState) => {
  let { socket } = getState()
  socket.emit('deleteLobby', id)
}
export const _findLobby = (id) => async (dispatch, getState) => {
  dispatch({ type: 'LOBBY_UPDATE', payload: { key: 'id', undefined } })
  let { socket } = getState()
  let { connected } = getState().app
  let timeout
  if (connected) {
    timeout = setTimeout(() => {
      socket.emit('findLobby', id, (callback) =>
        dispatch({
          type: 'LOBBY_UPDATE',
          payload: { key: 'id', value: callback },
        })
      )
    }, 2000)
  }
  return () => clearTimeout(timeout)
}

export const _joinLobby = (id) => async (dispatch, getState) => {
  let { socket } = getState()
  let { user } = getState()
  let lobbyId = getState().lobby.id
  if (lobbyId === id)
    socket.emit('joinLobby', { id, socketId: socket.id, user })
}
