import React, { useState } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { useContent } from '../../content/menu'

import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListSubheader from '@material-ui/core/ListSubheader'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Collapse from '@material-ui/core/Collapse'

// Icons
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import MenuIcon from '@material-ui/icons/Menu'

const useStyles = makeStyles(theme => ({
  menuButton: props => ({
    position: 'fixed',
    top: 16,
    right: 16,
    color: `rgba(${theme.palette[props.theme].reverse}, 0.8)`,
    '&:hover': {
      backgroundColor: `rgba(${theme.palette[props.theme].reverse}, 0.08)`
    }
  }),
  list: {
    width: 250
  },
  itemText: {
    paddingLeft: '70px',
    color: 'hsl(0, 0%, 50%)',
    backgroundColor: 'hsl(0, 0%, 20%)',
    '& div': {
      minWidth: '32px'
    },
    '& svg': {
      color: 'hsl(0, 0%, 50%)'
    },
    '&:hover': {
      '@media (hover: none)': {
        backgroundColor: 'hsl(0, 0%, 20%)'
      }
    }
  },
  activeText: props => ({
    color: theme.palette.primary[props.theme],
    '& svg': {
      color: theme.palette.primary[props.theme]
    }
  }),
  itemBullet: {
    width: '12px'
  }
}))

function SwipeableTemporaryDrawer(props) {
  const classes = useStyles(props)
  const content = useContent(props)
  const [isOpen, setOpen] = useState(false)
  const [expanded, setExpanded] = useState(false)
  const [anchor] = useState('right')

  const toggleDrawer = (anchor, open) => event => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return
    }

    setOpen(!isOpen)
  }

  const handleExpand = panel =>
    expanded === panel ? setExpanded(false) : setExpanded(panel)

  const list = anchor => (
    <div
      className={classes.list}
      role='presentation'
      onKeyDown={toggleDrawer(anchor, false)}>
      <List
        subheader={
          <ListSubheader component='div' id='nested-list-subheader'>
            KardGames
          </ListSubheader>
        }>
        {content.map((item, index) => (
          <div key={index}>
            <ListItem button onClick={() => handleExpand(index)}>
              <ListItemIcon>{item.icon}</ListItemIcon>
              <ListItemText primary={item.text} secondary={item.description} />
              {item.children &&
                (index === expanded ? <ExpandLess /> : <ExpandMore />)}
            </ListItem>
            {item.children && (
              <Collapse in={index === expanded} timeout='auto' unmountOnExit>
                <List component='div' disablePadding>
                  {item.children.map((item, index) => (
                    <ListItem
                      button
                      key={index}
                      className={
                        item.state && props[item.state[0]] === item.state[1]
                          ? `${classes.activeText} ${classes.itemText}`
                          : classes.itemText
                      }
                      onClick={
                        item.dispatch
                          ? () =>
                              props.dispatch({
                                type: item.dispatch.type,
                                payload: item.dispatch.payload
                              })
                          : null
                      }>
                      {item.icon ? (
                        <ListItemIcon>{item.icon}</ListItemIcon>
                      ) : (
                        <ListItemIcon>
                          <FiberManualRecordIcon
                            className={classes.itemBullet}
                            color='inherit'
                          />
                        </ListItemIcon>
                      )}
                      <ListItemText
                        primary={item.text}
                        secondary={item.description}
                      />
                    </ListItem>
                  ))}
                </List>
              </Collapse>
            )}
          </div>
        ))}
      </List>
    </div>
  )

  return (
    <div>
      <IconButton
        onClick={toggleDrawer(anchor, true)}
        className={classes.menuButton}>
        <MenuIcon fontSize='large' className={classes.menuIcon} />
      </IconButton>
      <SwipeableDrawer
        anchor={anchor}
        open={isOpen}
        onClose={toggleDrawer(anchor, false)}
        onOpen={toggleDrawer(anchor, true)}>
        {list(anchor)}
      </SwipeableDrawer>
    </div>
  )
}

const mapStateToProps = state => ({
  theme: state.app.theme,
  language: state.app.language
})

export default connect(mapStateToProps)(SwipeableTemporaryDrawer)
