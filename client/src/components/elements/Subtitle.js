/** @jsx jsx */
import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { s, colors } from '../../assets/style'
import { Typography, Grid } from '@material-ui/core'
import Animate from '../elements/Animate'

const Subtitle = ({ hide, ...props }) => (
  <Animate appear={props.appear}>
    <Grid container justify='center'>
      <Grid item xs={12} sm={10} md={7} lg={5} xl={4}>
        <Typography
          align='center'
          css={[style.subtitle, props.extraCSS]}
          component='h3'>
          {props.children}
        </Typography>
      </Grid>
    </Grid>
  </Animate>
)

const style = {
  subtitle: {
    fontFamily: 'Spartan',
    fontWeight: 400,
    marginLeft: '2rem',
    marginRight: '2rem',
    color: colors.text.subtitle,
    [s.xs]: {
      fontSize: '0.875rem',
      marginBottom: '2rem',
      lineHeight: 1.6 * 0.875 + 'rem',
    },
    [s.ss]: {
      fontSize: '0.75rem',
      lineHeight: 1.6 * 0.75 + 'rem',
    },
    [s.sm]: {
      fontSize: '1.125rem',
      marginBottom: '3rem',
      lineHeight: 1.6 * 1.125 + 'rem',
    },
    [s.lg]: {
      fontSize: '1.125rem',
      lineHeight: 1.6 * 1.125 + 'rem',
    },
    [s.xl]: { marginBottom: '4rem' },
  },
}

export default Subtitle
