/** @jsx jsx */
import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

import { ButtonBase } from '@material-ui/core'

import { s, colors } from '../../assets/style'

const ShortButton = ({
  text = 'TEXT',
  icon,
  fontSize = 0.75,
  color = colors.text.primary,
  background = 'transparent',
  hover = { opacity: 1 },
}) => {
  const style = {
    container: {
      fontSize: fontSize + 'rem',
      opacity: 0.75,
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      padding: '0.5rem 0.625rem',
      borderRadius: '2rem',
      transition: 'all 0.15s ease-out',
      color: color,
      backgroundColor: background,
      '&:hover': {
        hover,
      },
      '&:hover, &:focus': {
        opacity: 1,
      },
      '& svg': {
        fontSize: fontSize ? fontSize + 0.5 + 'rem' : 'inherit',
      },
    },
    text: {
      color: color,
      fontFamily: 'Spartan',
      fontSize: fontSize + 'rem',
      fontWeight: 700,
      padding: '0 0.25rem',
      [s.ss]: { fontSize: fontSize - 0.125 + 'rem' },
    },
  }

  return (
    <ButtonBase css={style.container}>
      <div css={style.text}>{text}</div>
      {icon}
    </ButtonBase>
  )
}

export default ShortButton
