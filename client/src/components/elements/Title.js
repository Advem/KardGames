/** @jsx jsx */
import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { s, colors } from '../../assets/style'
import { Typography } from '@material-ui/core'

const Title = (props) => (
  <Typography component='h1' align='center' css={style.title}>
    {props.children}
  </Typography>
)

const style = {
  title: {
    fontFamily: 'Spartan',
    fontWeight: 700,
    color: colors.text.title,
    [s.xs]: {
      fontSize: '2rem',
      marginBottom: '1rem',
    },
    [s.sm]: {
      fontSize: '2.5rem',
      marginBottom: '1.5rem',
    },
    [s.lg]: {
      fontSize: '3rem',
    },
  },
}

export default Title
