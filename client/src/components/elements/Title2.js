/** @jsx jsx */
import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux

// Components

// Style
import { Typography } from '@material-ui/core'
import { s, colors } from '../../assets/style'

const Title2 = (props) => (
  <Typography component='h2' align='left' css={style.title2}>
    {props.children}
  </Typography>
)

const style = {
  title2: {
    fontFamily: 'Spartan',
    fontWeight: 700,
    color: colors.text.title,
    [s.xs]: { fontSize: '1.25rem' },
    [s.ss]: { fontSize: '1.125rem' },
    [s.sm]: { fontSize: '1.25rem' },
    [s.lg]: { fontSize: '1.5rem' },
  },
}

export default Title2
