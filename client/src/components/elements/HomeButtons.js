/** @jsx jsx */
import React, { useEffect } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux
import { connect } from 'react-redux'

// Components
import HomeButton from '../elements/HomeButton'
import Animate from '../elements/Animate'

// Styling
import { s } from '../../assets/style'
import { text } from '../../content/language'

// Material UI
import { Grid } from '@material-ui/core'

// Icons
import PeopleIcon from '@material-ui/icons/People'
import GamesIcon from '@material-ui/icons/Games'

const HomeButtons = ({ language }) => {
  return (
    <Animate appear={true}>
      <Grid
        container
        alignItems='center'
        justify='center'
        css={style.homeButtons}>
        <HomeButton icon={<GamesIcon />} color='secondary' link='/create'>
          {text[language].home.createButton}
        </HomeButton>
        <HomeButton icon={<PeopleIcon />} color='primary' link='/join'>
          {text[language].home.joinButton}
        </HomeButton>
      </Grid>
    </Animate>
  )
}

const style = {
  homeButtons: {
    [s.xs]: { marginBottom: '1rem' },
    [s.sm]: { marginBottom: '2rem' },
    [s.lg]: { marginBottom: '3rem' },
    [s.xl]: { marginBottom: '3rem' },
  },
}

const mapStateToProps = (state) => ({ language: state.app.language })

export default connect(mapStateToProps)(HomeButtons)
