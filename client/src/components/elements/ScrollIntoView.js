import React, { useEffect, useRef } from 'react'

const ScrollIntoView = ({ trigger, to }) => {
  const containerEndRef = useRef(null)

  const scroll = () => {
    switch (to) {
      case 'view':
        containerEndRef.current.scrollIntoView({ behavior: 'smooth' })
        break
      case 'top':
        window.scrollTo({ top: 0, behavior: 'smooth' })
        break
      default:
        return null
    }
  }

  useEffect(scroll, [trigger])

  return <div ref={containerEndRef}></div>
}

export default ScrollIntoView
