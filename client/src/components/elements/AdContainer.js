/** @jsx jsx */
import React, { useState } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Style
import { Grid, IconButton } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { s, colors } from '../../assets/style'
import Animate from '../elements/Animate'

const AdContainer = () => {
  const [displayed, setDisplayed] = useState(true)

  return (
    <Animate appear={displayed}>
      <Grid container justify='center'>
        <Grid
          item
          xs={12}
          sm={10}
          lg={6}
          xl={6}
          // css={displayed ? style.adField : [style.adField, { opacity: 0 }]}
          css={style.adField}
          onClick={() => setDisplayed(!displayed)}>
          {displayed && (
            <IconButton size='small'>
              <CloseIcon fontSize='inherit' />
            </IconButton>
          )}
        </Grid>
      </Grid>
    </Animate>
  )
}

const style = {
  adField: {
    overflow: 'hidden',
    position: 'relative',
    borderRadius: '1rem',
    padding: '1rem',
    height: '7rem',
    backgroundColor: colors.background.dark,
    color: colors.text.secondary,
    [s.xs]: { margin: '0 1rem 2rem' },
    [s.sm]: { margin: '0 2rem 3rem' },
    [s.md]: { margin: '0 2rem 4rem' },
    button: {
      color: 'inherit',
      fontSize: '1.25rem',
      position: 'absolute',
      top: '0.5rem',
      right: '0.5rem',
    },
  },
}

export default AdContainer
