/** @jsx jsx */
import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { Link } from 'react-router-dom'

// Redux
import { connect } from 'react-redux'

// Material UI
import { Grid, IconButton, ButtonBase, Tooltip } from '@material-ui/core'
import MenuRoundedIcon from '@material-ui/icons/MenuRounded'

// Components
import DevTool from '../DevTool'

// Styles
import { s, colors } from '../../assets/style'
import logo from '../../assets/icons/KardGames'

const Header = (props) => {
  return (
    <header>
      <Grid container css={style.header}>
        <Grid item>
          <ButtonBase css={style.header.logo}>
            <Link to='/'>{logo}</Link>
          </ButtonBase>
        </Grid>
        <Grid item>
          <Grid container alignItems='center' justify='space-between'>
            <Grid item>
              <DevTool />
            </Grid>
            <Grid
              item
              onClick={() => props.dispatch({ type: 'APP_TOGGLE_LANGUAGE' })}>
              <Tooltip title='Menu'>
                <IconButton color='inherit'>
                  <MenuRoundedIcon />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </header>
  )
}

const style = {
  header: {
    top: 0,
    zIndex: 9999,
    width: '100vw',
    height: '4rem',
    padding: '0 1rem',
    position: 'fixed',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    // boxShadow: `0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)`,
    backgroundColor: colors.background.dark,
    [s.md]: { backgroundColor: colors.background.darker },
    logo: {
      color: colors.main.primary,
      backgroundColor: 'transparent',
      width: '3rem',
      height: '3rem',
      borderRadius: '50%',
      '& svg': {
        width: '2.5rem',
        height: 'auto',
      },
    },
  },
}

export default connect()(Header)
