/** @jsx jsx */

import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { Fade } from '@material-ui/core'
import { Collapse } from '@material-ui/core'

const Animate = ({
  appear,
  hide,
  fadeEnter = 1000,
  fadeExit = 400,
  collEnter = 1000,
  collExit = 1000,
  extraCSS,
  ...props
}) => {
  // console.log(`Animate received ${appear}`)

  if (appear !== undefined)
    return (
      <Collapse
        in={appear}
        timeout={{ enter: collEnter, exit: collExit }}
        css={extraCSS}>
        <Fade
          in={appear}
          timeout={{ enter: fadeEnter, exit: fadeExit }}
          css={extraCSS}>
          {props.children}
        </Fade>
      </Collapse>
    )
  else return props.children
}

export default Animate
