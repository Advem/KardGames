/** @jsx jsx */
import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { s } from '../../assets/style'
import { Grid } from '@material-ui/core'
import Animate from '../elements/Animate'

const InfoText = ({ hide, ...props }) => (
  <Animate
    appear={props.appear}
    extraCSS={{ width: '100%' }}
    fadeEnter={1000}
    collEnter={400}
    fadeExit={0}
    collExit={400}>
    <Grid container justify='center'>
      <Grid item xs={12} sm={10}>
        <div css={[style, props.extraCSS]}>{props.children}</div>
      </Grid>
    </Grid>
  </Animate>
)

const style = {
  color: 'inherit',
  backgroundColor: 'transparent',
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  transition: 'all 0.3s ease-out',
  minHeight: 40,
  fontFamily: 'Spartan',
  fontWeight: 400,
  [s.xs]: { fontSize: '0.75rem', marginTop: '0rem' },
  [s.sm]: { marginTop: '0.75rem' },
  // [s.md]: { fontSize: '0.875rem' },
  '& span': {
    padding: '0 0.5rem',
  },
  '& svg': {
    color: 'currentColor',
    fontSize: '1.25rem',
  },
}

export default InfoText
