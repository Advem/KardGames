/** @jsx jsx */
import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { Grid, CircularProgress } from '@material-ui/core'

const Loader = ({
  size = '2rem',
  padding = '1rem',
  color = 'inherit',
  thickness = 3.6
}) => (
  <Grid item xs={12} css={{ padding: padding }}>
    <Grid container direction='column' justify='center' alignItems='center'>
      <CircularProgress size={size} thickness={thickness} color={color} />
    </Grid>
  </Grid>
)

export default Loader
