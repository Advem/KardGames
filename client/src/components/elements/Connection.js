/** @jsx jsx */
import React, { useState, useEffect } from 'react' // eslint-disable-line
import { jsx, css } from '@emotion/core' // eslint-disable-line
import { connect } from 'react-redux'
import { text } from '../../content/language'
import { Snackbar } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

const Connection = (props) => {
  const [open, setOpen] = useState(true)
  const [currConnected, setConnected] = useState(false)
  const handleClose = () =>
    props.connected && props.connected === currConnected
      ? setOpen(false)
      : setOpen(true)

  useEffect(() => {
    if (props.connected !== currConnected) setOpen(true)
  }, [props.connected, currConnected, setConnected])

  useEffect(() => {
    setConnected(props.connected)
  }, [props.connected, setConnected])

  return (
    <Snackbar
      open={open}
      autoHideDuration={props.connected ? 6000 : null}
      onClose={handleClose}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      css={{ bottom: '1rem' }}>
      {props.connected ? (
        <Alert onClose={handleClose} severity='success' variant='filled'>
          {text[props.language].connection.success}
        </Alert>
      ) : (
        <Alert severity='error' variant='filled'>
          {text[props.language].connection.error}
        </Alert>
      )}
    </Snackbar>
  )
}

const mapStateToProps = (state) => ({
  connected: state.app.connected,
  language: state.app.language,
})

export default connect(mapStateToProps)(Connection)
