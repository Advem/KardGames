/** @jsx jsx */
import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { Link } from 'react-router-dom'

import { Grid, Typography, Button } from '@material-ui/core'
import { s, colors } from '../../assets/style'

const HomeButton = (props) => (
  <Grid item>
    <Link to={props.link}>
      <Button
        variant='contained'
        color={props.color}
        startIcon={props.icon}
        css={style.homeButton}>
        <Typography component='h3'>{props.children}</Typography>
      </Button>
    </Link>
  </Grid>
)

const style = {
  homeButton: {
    color: colors.background.darker,
    borderRadius: '4rem',
    padding: '1rem 1.5rem',
    [s.xs]: { margin: '0 1rem 1.5rem 1rem' },
    [s.sm]: { margin: '0 4rem 2rem 4rem' },
    '&:hover': {
      background: colors.background.lighter,
    },
    '& h3': {
      fontFamily: 'Spartan',
      fontWeight: 700,
      // letterSpacing: 2,
      // paddingLeft: '0.5rem',
      // textShadow: '0px 2px 4px rgba(0, 0, 0, .3)',
      [s.xs]: { fontSize: '1rem' },
      [s.sm]: { fontSize: '1.25rem' },
      [s.md]: { fontSize: '1.25rem', padding: '0.5rem 0.5rem' },
      [s.lg]: { fontSize: '1.25rem', padding: '0.75rem 0.5rem' },
    },
    '& svg': {
      // filter: 'drop-shadow( 0px 2px 4px rgba(0, 0, 0, .4))',
      [s.xs]: { width: '1.75rem', height: 'auto' },
      [s.sm]: { width: '2.00rem', height: 'auto' },
      [s.md]: { width: '2.25rem', height: 'auto' },
      // [lg]: { width: '2.50rem', height: 'auto' },
    },
  },
}

export default HomeButton
