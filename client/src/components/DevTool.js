/** @jsx jsx */
import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { connect } from 'react-redux'
import { Grid } from '@material-ui/core'

// Icons
import PersonIcon from '@material-ui/icons/Person'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import CancelIcon from '@material-ui/icons/Cancel'
import HelpIcon from '@material-ui/icons/Help'
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked'

const DevTool = ({ dispatch, userName }) => (
  <Grid container justify='center' alignItems='center' css={style}>
    <span>DEV</span>
    <button onClick={() => dispatch({ type: 'LOBBY_SET_ID', payload: null })}>
      <RadioButtonCheckedIcon />
    </button>
    <button
      onClick={() =>
        dispatch({
          type: 'LOBBY_SET_ID',
          payload: undefined,
        })
      }>
      <HelpIcon />
    </button>
    <button onClick={() => dispatch({ type: 'LOBBY_SET_ID', payload: false })}>
      <CancelIcon />
    </button>
    <button
      onClick={() =>
        dispatch({
          type: 'LOBBY_SET_ID',
          payload: 'TEST',
        })
      }>
      <CheckCircleIcon />
    </button>
    <button
      onClick={() =>
        userName
          ? dispatch({
              type: 'USER_SET_STATE',
              payload: {
                name: null,
                color: null,
                wins: null,
                losses: null,
              },
            })
          : dispatch({
              type: 'USER_SET_STATE',
              payload: {
                name: 'Admin',
                color: '#ff00ff',
                wins: 2,
                losses: 13,
              },
            })
      }>
      <PersonIcon />
    </button>
  </Grid>
)

const style = {
  span: {
    opacity: 0.25,
    fontWeight: 'bold',
    paddingRight: 0.5 + 'rem',
    fontSize: 1 + 'rem',
  },
  button: {
    cursor: 'pointer',
    padding: '0 0.125rem',
    apperance: 'none',
    background: 'transparent',
    outline: 'none',
    border: 'none',
    opacity: 0.25,
    '&:hover': {
      opacity: 1,
    },
    '& svg': {
      color: 'white',
    },
  },
}

const mapStateToProps = (state) => ({
  userName: state.user.name,
})

export default connect(mapStateToProps)(DevTool)
