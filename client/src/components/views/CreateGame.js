/** @jsx jsx */
import React, { useEffect } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux
import { connect } from 'react-redux'
import { _setLobbyId } from '../../redux/actions'

// Components
import Title from '../elements/Title'
import Subtitle from '../elements/Subtitle'
import Games from '../lobby/Games'
import AdContainer from '../elements/AdContainer'
import LobbyID from '../lobby/LobbyID'
import UserForm from '../lobby/UserForm'
import LobbyMain from '../lobby/LobbyMain'

// Styling
import { text } from '../../content/language'

// Material UI

// Icons

const CreateGame = ({
  language,
  lobbyGame,
  userName,
  stage,
  _setLobbyId,
  ...props
}) => {
  // Init Lobby with ID
  useEffect(() => {
    const ID = Math.random().toString(36).substring(2, 8).toLocaleUpperCase()
    _setLobbyId(ID)
  }, [_setLobbyId])

  return (
    <div>
      <Title>{text[language].create.title}</Title>
      <Subtitle appear={true}>{text[language].create.subtitle}</Subtitle>
      <Games appear={true} />
      <UserForm appear={Boolean(lobbyGame && !userName)} />
      {lobbyGame && userName && (
        <LobbyID appear={Boolean(lobbyGame && userName)} />
        /* NOT SURE IF CAN BE MOUNTED */
      )}
      <LobbyMain appear={Boolean(lobbyGame && userName)} />
      <AdContainer />
    </div>
  )
}

const mapStateToProps = (state) => ({
  language: state.app.language,
  lobbyGame: state.lobby.game,
  userName: state.user.name,
})

export default connect(mapStateToProps, { _setLobbyId })(CreateGame)
