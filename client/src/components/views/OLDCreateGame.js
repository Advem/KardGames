/** @jsx jsx */
import React, { useEffect, useState, useRef } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { text } from '../../content/language'
import { clientURL } from '../../utils/endpoints'
import {
  _createLobby,
  _deleteLobby,
  _setLobbyHost,
  _setLobbyId,
} from '../../redux/actions'

import Games from '../lobby/Games'
import Players from '../lobby/Players'

import { Grid, Typography, ButtonBase } from '@material-ui/core'
import Snackbar from '@material-ui/core/Snackbar'
import { Alert } from '@material-ui/lab'

const CreateGame = ({
  dispatch,
  lobby,
  connected,
  _setLobbyHost,
  _setLobbyId,
  _createLobby,
  _deleteLobby,
  ...props
}) => {
  const [linkCopied, setCopied] = useState(false)
  const [isGame, setIsGame] = useState(false)
  const gameLink = useRef(null)

  useEffect(() => {
    const ID = Math.random().toString(36).substring(2, 8).toLocaleUpperCase()
    _setLobbyId(ID)
  }, [_setLobbyId])

  useEffect(() => {
    if (connected) _setLobbyHost()
  }, [connected, _setLobbyHost])

  useEffect(() => {
    if (connected && lobby.id && isGame) _createLobby()
  }, [connected, lobby.id, isGame, _createLobby])

  useEffect(() => {
    return () => {
      if (lobby.id) _deleteLobby(lobby.id)
    }
  }, [lobby.id, _deleteLobby])

  useEffect(() => {
    if (lobby.game) setIsGame(true)
  }, [lobby.game, setIsGame])

  // must react to reconnect

  const handleLinkClick = () => {
    const link = `${clientURL}/join/${gameLink.current.innerHTML}`
    navigator.clipboard.writeText(link).then(
      () => {
        if (!linkCopied) setCopied(!linkCopied)
      },
      () => {
        setCopied(false)
      }
    )
  }

  const handleLinkClose = (reason) => {
    if (reason !== 'clickaway') setCopied(false)
  }

  const LobbyId = () => (
    <Grid container justify='center'>
      <Grid
        item
        xs={12}
        sm={6}
        md={4}
        lg={3}
        xl={2}
        onClick={() => handleLinkClick()}>
        <ButtonBase css={(theme) => style(theme, props).gameIdField}>
          <Typography variant='h2' component='h3' align='center' ref={gameLink}>
            {lobby.id}
          </Typography>
        </ButtonBase>
      </Grid>
    </Grid>
  )

  const LinkCopied = () => (
    <Grid container>
      <Grid item xs>
        <Typography
          variant='body2'
          align='center'
          css={(theme) => style(theme, props).gameIdText}>
          {linkCopied
            ? text[props.language].create.linkCopied
            : text[props.language].create.linkToCopy}
        </Typography>
      </Grid>
      <Snackbar
        open={linkCopied}
        autoHideDuration={3000}
        onClose={(event, reason) => handleLinkClose(reason)}>
        <Alert onClose={() => handleLinkClose()} color='info' variant='filled'>
          {text[props.language].create.linkCopied}
        </Alert>
      </Snackbar>
    </Grid>
  )

  const Headline = () => (
    <Typography
      css={(theme) => style(theme, props).header}
      variant='h4'
      component='h1'
      align='center'
      gutterBottom>
      {text[props.language].create.header}
    </Typography>
  )

  return (
    <div>
      {Headline()}
      <Games />
      {lobby.game && (
        <div>
          {LobbyId()}
          {LinkCopied()}
          <Players />
        </div>
      )}
    </div>
  )
}

const style = (theme, props) => ({
  header: {
    opacity: 0.2,
    margin: '0rem 0 1rem 0',
  },
  gameIdField: {
    width: 'calc(100% - 1.6rem)',
    backgroundColor: `rgba(${theme.palette[props.theme].reverse}, 0.08)`,
    padding: '1rem 0',
    margin: '0.8rem',
    borderRadius: '0.67rem',
    boxShadow:
      '0px 3px 3px -2px rgba(0,0,0,0.2), 0px 3px 4px 0px rgba(0,0,0,0.14), 0px 1px 8px 0px rgba(0,0,0,0.12)',
    cursor: 'pointer',
  },
  gameIdText: { opacity: 0.5, padding: '0.5rem' },
})

const mapStateToProps = (state) => ({
  language: state.app.language,
  theme: state.app.theme,
  lobby: state.lobby,
  connected: state.app.connected,
})

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { _createLobby, _deleteLobby, _setLobbyHost, _setLobbyId, dispatch },
    dispatch
  )
}
export default connect(mapStateToProps, mapDispatchToProps)(CreateGame)
