/** @jsx jsx */
import React, { useEffect, useState } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux
import { connect } from 'react-redux'
import {
  _getLobbies,
  _joinLobby,
  _deleteLobbyPlayer,
} from '../../redux/actions'

// Components
import Title from '../elements/Title'
import Subtitle from '../elements/Subtitle'
import AdContainer from '../elements/AdContainer'
import LobbyForm from '../lobby/LobbyForm'
import UserForm from '../lobby/UserForm'
import OpenLobbies from '../lobby/OpenLobbies'
import ScrollIntroView from '../elements/ScrollIntoView'
import LobbyMain from '../lobby/LobbyMain'

// Style
import { text } from '../../content/language'
import { useParams } from 'react-router-dom'
import { bindActionCreators } from 'redux'

const JoinGame = ({
  _getLobbies,
  _joinLobby,
  _deleteLobbyPlayer,
  stage,
  dispatch,
  lobbyId,
  userName,
  connected,
  ...props
}) => {
  const [URL, setURL] = useState(null)
  let getUrl = useParams().lobby

  // // Abandoned
  // useEffect(() => {
  //   const getUrl = window.location.href
  //     .split('/join')
  //     .pop()
  //     .substring(1)
  //     .toLocaleUpperCase()
  //     .replace(/[^a-zA-Z0-9]/g, '')
  //   if (getUrl) {
  //     setURL(getUrl)
  //   }
  // }, [setURL])

  useEffect(() => {
    if (getUrl) {
      setURL(getUrl.toLocaleUpperCase().replace(/[^a-zA-Z0-9]/g, ''))
    } else dispatch({ type: 'LOBBY_SET_ID', payload: null })
  }, [dispatch, getUrl, setURL])

  useEffect(() => {
    return () => stage !== 'game' && _deleteLobbyPlayer()
  }, [_deleteLobbyPlayer, stage])

  useEffect(() => {
    if (connected && lobbyId && userName) _joinLobby(lobbyId)
  }, [connected, lobbyId, userName, _joinLobby])

  useEffect(() => {
    if (connected && !lobbyId) _getLobbies()
  }, [_getLobbies, connected, lobbyId])

  return (
    <div>
      <ScrollIntroView trigger={lobbyId} to={'top'} />
      <Title>{text[props.language].join.title}</Title>
      <Subtitle appear={!lobbyId}>
        {text[props.language].join.subtitle}
      </Subtitle>
      <LobbyForm initial={URL} />
      <UserForm appear={Boolean(lobbyId && !userName)} />
      <LobbyMain appear={Boolean(lobbyId && userName)} />
      <AdContainer />
      <OpenLobbies appear={!lobbyId} />
    </div>
  )
}

const mapStateToProps = (state) => ({
  language: state.app.language,
  lobbyId: state.lobby.id,
  connected: state.app.connected,
  userName: state.user.name,
  stage: state.app.stage,
})

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { _getLobbies, _joinLobby, _deleteLobbyPlayer, dispatch },
    dispatch
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(JoinGame)
