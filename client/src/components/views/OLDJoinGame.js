/** @jsx jsx */
import React, { useEffect, useState } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { connect } from 'react-redux'
import { text } from '../../content/language'
import {
  _setLobbyId,
  _findLobby,
  _deleteLobbyPlayer,
} from '../../redux/actions'

import Header from '../elements/Header'
import Players from '../lobby/Players'
import Game from '../lobby/Game'
import Loader from '../elements/Loader'
import CustomizePlayer from '../lobby/CustomizePlayer' // eslint-disable-line no-unused-vars

import { Grid, Typography, ButtonBase } from '@material-ui/core'

const style = (theme, props) => ({
  header: {
    width: '100%',
    textAlign: 'center',
    opacity: 0.2,
    // margin: '2rem 0 2rem 0'
  },
  inputField: {
    transitionDuration: '0.3s',
    opacity: 0.5,
    width: '100%',
    textAlign: 'center',
    borderRadius: '0.67rem',
    padding: '1rem 0',
    appearance: 'none',
    border: 'none',
    cursor: 'pointer',
    color:
      props.lobby.id === false
        ? theme.palette.error[props.theme]
        : `rgba(${theme.palette[props.theme].reverse}, 1)`,
    backgroundColor:
      props.lobby.id === false
        ? `rgba(255,0,0,0.2)`
        : `rgba(${theme.palette[props.theme].reverse}, 0.16)`,
    boxShadow:
      ' 0px 3px 3px -2px rgba(0,0,0,0.2),  0px 3px 4px 0px rgba(0,0,0,0.14),  0px 1px 8px 0px rgba(0,0,0,0.12)',
    '&': theme.typography.h2,
    '&::placeholder': {
      transitionDuration: '0.3s',
      opacity: 1,
      color: props.lobby.id === false ? '#D32F2F' : 'currentColor',
    },
    '&:focus': {
      outline: 'none',
      opacity: 1,
      '&::placeholder': { opacity: 0.12 },
    },
  },
})

const s = {
  margin: {
    margin: '2rem 0',
  },
  error: {
    color: '#D32F2F',
  },
}

const JoinGame = ({
  _setLobbyId,
  _findLobby,
  _deleteLobbyPlayer,
  connected,
  ...props
}) => {
  const [URL, setURL] = useState(null)
  const [lobbyTemp, setLobbyTemp] = useState(null)
  const [inputLobbyValue, setInputLobbyValue] = useState('')

  useEffect(() => {
    const url = window.location.href
      .split('/join')
      .pop()
      .substring(1)
      .toLocaleUpperCase()
    if (url) {
      setURL(url)
      setLobbyTemp(url)
      if (connected) _findLobby(url)
    }
    // else _setLobbyId(null)
  }, [connected, _findLobby, setLobbyTemp])

  useEffect(() => {
    return () => {
      _deleteLobbyPlayer()
    }
  }, [_deleteLobbyPlayer])

  useEffect(() => {
    if (
      props.lobby.id === false ||
      (props.lobby.id && props.lobby.id !== undefined)
    )
      setLobbyTemp(null)
  }, [props.lobby.id, setLobbyTemp])

  const handleInputLobbyChange = (event) => {
    setLobbyTemp(null)
    if (props.lobby.id === false) _setLobbyId(null)

    setInputLobbyValue(event.target.value.toUpperCase())
    if (event.target.value.toUpperCase().length === 6) {
      _findLobby(event.target.value.toUpperCase())
    }
  }

  const handleInputLobbySubmit = (event) => {
    event.preventDefault()
  }

  const LobbyInput = () => (
    <form autoComplete='off' css={s.margin} onSubmit={handleInputLobbySubmit}>
      <Grid item>
        <Grid container justify='center'>
          <Grid item xs={11} sm={6}>
            <ButtonBase color='primary'>
              <input
                type='text'
                placeholder='Lobby ID'
                maxLength='6'
                value={inputLobbyValue}
                onChange={handleInputLobbyChange}
                css={(theme) => style(theme, props).inputField}
              />
            </ButtonBase>
          </Grid>
        </Grid>
      </Grid>
    </form>
  )

  const Headline = () => (
    <Grid item xs={11}>
      <Typography
        css={(theme) => style(theme, props).header}
        variant='h4'
        component='h1'
        gutterBottom>
        {/* {props.lobby.id
          ? props.lobby.id === 'searching'
            ? text[props.language].join.header
            : `${text[props.language].join.lobby} ${props.lobby.id}`
          : text[props.language].join.header} */}
        {(!props.lobby.id || props.lobby.id === undefined) &&
          text[props.language].join.header}
      </Typography>
    </Grid>
  )

  const TextLobby = (type) => (
    <Grid item xs={11} css={s[`${type}`]}>
      <Typography
        variant='subtitle1'
        gutterBottom
        align='center'
        color='inherit'>
        {text[props.language].join[`${type}`]}
      </Typography>
      <Typography variant='h3' gutterBottom align='center' color='inherit'>
        {inputLobbyValue || lobbyTemp || URL}
      </Typography>
    </Grid>
  )

  // const renderStatus = () =>
  //   !lobbyTemp
  //     ? !lobby.id && Header()
  //     : lobby.id === null
  //     ? TextLobby('loading')
  //     : !lobby.id && TextLobby('error')

  return (
    <Grid container justify='center' alignItems='center'>
      {Headline()}
      {!props.lobby.id && !lobbyTemp && LobbyInput()}
      {props.lobby.id === false && TextLobby('error')}
      {(lobbyTemp || props.lobby.id === undefined) &&
        props.lobby.id !== false && <Loader size='4.5rem' padding='3rem 0' />}
      {(lobbyTemp || props.lobby.id === undefined) &&
        props.lobby.id !== false &&
        TextLobby('loading')}
      {props.lobby.game && <Game />}
      {props.lobby.id && props.lobby.id !== undefined && <Players />}
    </Grid>
  )
}

const mapStateToProps = (state) => ({
  language: state.app.language,
  theme: state.app.theme,
  lobby: state.lobby,
  connected: state.app.connected,
})

export default connect(mapStateToProps, {
  _setLobbyId,
  _findLobby,
  _deleteLobbyPlayer,
})(JoinGame)
