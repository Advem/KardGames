/** @jsx jsx */
import React, { useEffect } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux
import { connect } from 'react-redux'

// Components
import Title from '../elements/Title'
import Subtitle from '../elements/Subtitle'
import HomeButtons from '../elements/HomeButtons'

// Style
import { text } from '../../content/language'
import AdContainer from '../elements/AdContainer'

const Home = ({ dispatch, ...props }) => {
  useEffect(() => {
    dispatch({ type: 'LOBBY_RESET' })
  }, [dispatch])

  return (
    <div>
      <Title>{text[props.language].home.title}</Title>
      <Subtitle appear={true}>{text[props.language].home.subtitle}</Subtitle>
      <AdContainer />
      <HomeButtons />
    </div>
  )
}

const mapStateToProps = (state) => ({ language: state.app.language })

export default connect(mapStateToProps)(Home)
