/** @jsx jsx */
import React, { useEffect } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { _updateLobby, _updateLobbyL } from '../../redux/actions'

// Components
import Animate from '../elements/Animate'

// Styling
import {
  availableGames,
  icons as gameIcons,
  names as gameNames,
  colors as gameColors,
} from '../../content/games'
import { colors, s, alpha } from '../../assets/style'

// Material UI
import { Grid, ButtonBase } from '@material-ui/core'

const Games = ({
  _updateLobby,
  _updateLobbyL,
  language,
  gameState,
  userName,
  appear,
}) => {
  let delay
  // MAKE HOOK LATER
  useEffect(() => {
    if (userName)
      // eslint-disable-next-line
      delay = setTimeout(() => {
        _updateLobby('game', gameState)
      }, 3000)

    return () => clearTimeout(delay)
  }, [gameState, userName, _updateLobby, delay])

  const handleGameChange = (game) => {
    if (gameState !== game) {
      _updateLobbyL('game', game)
    }
  }

  const renderGames = () =>
    availableGames.map((game, index) => (
      <Grid item key={index} onClick={() => handleGameChange(game)}>
        <ButtonBase
          focusRipple
          css={[
            style.item,
            gameState === null
              ? colorize(game, false, 'initial')
              : gameState === game
              ? colorize(game, true)
              : colorize(game, false),
          ]}>
          <div>{gameIcons[game] || gameIcons.unknown}</div>
          <h2>
            {gameNames[game]
              ? gameNames[game][language]
              : gameNames.unknown[language]}
          </h2>
        </ButtonBase>
      </Grid>
    ))

  return (
    <Animate appear={appear}>
      <Grid container wrap='nowrap' alignItems='center' css={style.container}>
        {renderGames()}
      </Grid>
    </Animate>
  )
}

const style = {
  container: {
    overflowX: 'auto',
    WebkitOverflowScrolling: 'touch',
    padding: '0 0.5rem',
    margin: 0,
    [s.xs]: { marginBottom: '1rem' },
    [s.sm]: {
      marginBottom: '1.25rem',
      '@media (hover: hover)': {
        justifyContent: 'center',
        flexWrap: 'wrap',
        padding: '0',
      },
    },
    [s.lg]: { marginBottom: '2rem' },
  },
  item: {
    width: 120,
    height: 160,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    margin: '0 0.5rem 1rem',
    borderRadius: '0.75rem',
    '& svg': {
      width: 100,
      height: 100,
      padding: '1rem',
      transition: 'padding 0.15s ease-out',
    },
    '& h2': {
      maxWidth: 120,
      padding: 0,
      margin: 0,
      marginBottom: '1rem',
      fontFamily: 'Spartan',
      paddingBottom: '0',
      fontSize: '1.125rem',
      letterSpacing: '-1px',
      fontWeight: 'bold',
      textTransform: 'capitalize',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'clip',
    },
  },
  initailColors: {
    backgroundColor: colors.background.dark.concat(alpha[50]),
    color: colors.text.subtitle.concat(alpha[40]),
  },
}

const colorize = (game, active, initial = false) => {
  let color = gameColors[game] ? game : 'unknown'
  return [
    {
      transition: 'all 0.15s ease-out',
    },
    active
      ? {
          color: gameColors[color],
          backgroundColor: gameColors[color].concat(alpha[25]),
          '& svg': {
            padding: '0.5rem',
          },
        }
      : [
          initial && {
            [s.xs]: {
              color: colors.text.secondary.concat(alpha[100]),
              backgroundColor: colors.background.light.concat(alpha[100]),
            },
          },
          {
            color: colors.text.subtitle.concat(alpha[25]),
            backgroundColor: colors.background.dark.concat(alpha[100]),
            '&:hover': {
              color: gameColors[color].concat(alpha[25]),
              backgroundColor: gameColors[color].concat(alpha[10]),
            },
          },
        ],
  ]
}

const mapStateToProps = (state) => ({
  language: state.app.language,
  gameState: state.lobby.game,
  userName: state.user.name,
})

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ _updateLobby, _updateLobbyL }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Games)
