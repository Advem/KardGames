/** @jsx jsx */
import React, { useEffect, useState } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux
import { connect } from 'react-redux'

// Styling
import { colors, alpha } from '../../assets/style'
import { text } from '../../content/language'

// Components
import ShortButton from '../elements/ShortButton'

// Material
import {
  Popper,
  Grow,
  Paper,
  ClickAwayListener,
  ButtonBase,
  MenuList,
} from '@material-ui/core'

// Icons
import SortIcon from '@material-ui/icons/Sort'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'

const SortMenu = ({ sortMethod, setSortMethod, language }) => {
  const [open, setOpen] = React.useState(false)
  const anchorRef = React.useRef(null)

  const handleToggle = () => setOpen((prevOpen) => !prevOpen)

  const handleClose = (event) =>
    anchorRef.current && anchorRef.current.contains(event.target)
      ? null
      : setOpen(false)

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault()
      setOpen(false)
    }
  }

  const sortMethods = ['players', 'games', 'hosts', 'newest']

  return (
    <React.Fragment>
      <div
        ref={anchorRef}
        aria-controls={open ? 'menu-list-grow' : undefined}
        aria-haspopup='true'
        onClick={handleToggle}>
        <ShortButton
          background={
            open ? colors.background.light.concat(alpha[100]) : 'transparent'
          }
          text={text[language].openLobbies.sort}
          icon={<SortIcon />}
          fontSize={0.875}></ShortButton>
      </div>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        css={{
          zIndex: 1,
          left: '-0.5rem !important',
          // transition: 'transform 0.15s ease-out',
        }}>
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === 'bottom' ? 'center top' : 'center bottom',
            }}>
            <Paper css={{ borderRadius: '1rem' }} elevation={12}>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList
                  css={{ padding: 0 }}
                  autoFocusItem={open}
                  id='menu-list-grow'
                  onKeyDown={handleListKeyDown}>
                  {sortMethods.map((method) => (
                    <div key={method} css={style.sort.item}>
                      <ButtonBase
                        css={[style.sort.btn, style.sort.btn.L]}
                        onClick={() =>
                          setSortMethod(`${method}DESC`)
                        }></ButtonBase>
                      <ButtonBase
                        css={[style.sort.btn, style.sort.btn.R]}
                        onClick={() =>
                          setSortMethod(`${method}ASC`)
                        }></ButtonBase>
                      <div css={style.sort.text}>
                        <KeyboardArrowDownIcon
                          css={
                            sortMethod === `${method}DESC` &&
                            style.sort.activeIcon
                          }
                        />
                        <span
                          css={
                            (sortMethod === `${method}ASC` ||
                              sortMethod === `${method}DESC`) &&
                            style.sort.activeText
                          }>
                          {text[language][method]}
                        </span>
                        <KeyboardArrowUpIcon
                          css={
                            sortMethod === `${method}ASC` &&
                            style.sort.activeIcon
                          }
                        />
                      </div>
                    </div>
                  ))}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </React.Fragment>
  )
}

const style = {
  sort: {
    item: {
      borderRadius: '1rem',
      // backgroundColor: colors.background.light,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'relative',
      alignItems: 'center',
      '&:focus': { outline: 'none' },
    },
    text: {
      minWidth: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      pointerEvents: 'none',
      padding: '0.5rem 0.5rem',
      fontSize: '0.875rem',
      fontWeight: 'bold',
      textTransform: 'capitalize',
      color: colors.text.secondary.concat(alpha[50]),
      '& span': {
        padding: '0 0.5rem',
      },
    },
    btn: {
      transition: 'background-color 0.3s ease-out',
      minWidth: '50%',
      minHeight: '100%',
      position: 'absolute',
      color: colors.main.primary,
      L: { left: 0, borderRadius: '1rem 0 0 1rem' },
      R: { right: 0, borderRadius: '0 1rem 1rem 0' },
      '@media (hover: hover)': {
        '&:hover': {
          backgroundColor: colors.main.primary.concat(alpha[50]),
        },
      },
    },
    activeIcon: {
      backgroundColor: colors.main.primary.concat(alpha[75]),
      color: 'white',
      borderRadius: '1rem',
    },
    activeText: {
      color: 'white',
    },
  },
}

const mapStateToProps = (state) => ({
  language: state.app.language,
})

export default connect(mapStateToProps)(SortMenu)
