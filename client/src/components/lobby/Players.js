/** @jsx jsx */
import React, { useEffect, useRef } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { connect } from 'react-redux'
import { text } from '../../content/language'

import { Grid, Avatar, Typography, Chip, Hidden } from '@material-ui/core'

// Icons
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import SentimentVeryDissatisfiedSharpIcon from '@material-ui/icons/SentimentVeryDissatisfiedSharp'
import HomeIcon from '@material-ui/icons/Home'
import StarIcon from '@material-ui/icons/Star'
import ThumbUpIcon from '@material-ui/icons/ThumbUp'
import ThumbDownIcon from '@material-ui/icons/ThumbDown'

const style = (theme, props) => ({
  item: {
    width: '100%',
    margin: '0.5rem 0',
    borderRadius: '2rem',
    padding: '1rem 0',
    paddingLeft: '1rem',
    backgroundColor: props.theme
      ? `rgba(${theme.palette[props.theme].reverse}, 0.04)`
      : `rgba(0,0,0,0.5)`,
    background:
      props.theme &&
      props.ready &&
      `linear-gradient(120deg, rgba(${
        theme.palette[props.theme].reverse
      }, 0.04) 60%, ${theme.palette.primary.dark}4d)`
    // `linear-gradient(75deg, rgba(${
    //   theme.palette[props.theme].reverse
    // }, 0.04) 72%, ${theme.palette.primary.dark}4d 0%)`,
  },
  avatar: {
    backgroundColor:
      props.color ||
      (props.theme && `rgba(${theme.palette[props.theme].reverse}, 0.2)`),
    color: props.theme ? theme.palette.background[props.theme] : 'inherit',
    fontWeight: 600
  },
  name: {
    paddingRight: '0.5rem',
    opacity: props.name ? 1 : 0.4,
    fontWeight: props.name ? 400 : 300
  },
  chip: {
    border: 'none',
    justifySelf: 'flex-end',
    marginRight: '0.5rem',
    color:
      props.theme &&
      (props.isWin
        ? theme.palette.primary[props.theme]
        : theme.palette.error[props.theme]),
    '& span': {
      fontSize: '1rem',
      fontWeight: 600,
      paddingLeft: '0.75rem',
      paddingRight: '0.75rem'
    },
    '& svg': {
      color: 'currentColor',
      opacity: 0.5
    }
  },
  icon: {
    user: {
      fontSize: '0.75rem',
      opacity: 0.6,
      color: theme.palette.primary[props.theme]
    },
    host: {
      paddingRight: '0.25rem',
      fontSize: '1rem',
      opacity: 0.4
    }
  },
  ready: {
    borderRadius: '0 2rem 2rem 0',
    color:
      props.theme &&
      (props.ready
        ? theme.palette.primary.dark
        : `rgba(${theme.palette[props.theme].reverse}, 0.12)`),
    '& p': {
      textAlign: 'right',
      textTransform: 'uppercase',
      fontSize: '2rem',
      fontWeight: 300
    },
    '& svg': {
      padding: '0 1.5rem 0 1rem'
    }
  }
})

const Players = ({ players, host, socket, ...props }) => {
  const containerEndRef = useRef(null)

  const scrollToBottom = () => {
    containerEndRef.current.scrollIntoView({ behavior: 'smooth' })
  }

  useEffect(scrollToBottom, [players])

  return (
    <Grid container direction='column' justify='center' alignItems='center'>
      {players &&
        Object.entries(players).map(
          ([key, { name, color, ready, wins, losses }]) => (
            <Grid
              item
              key={key}
              xs={11}
              sm={11}
              md={8}
              lg={6}
              xl={4}
              css={theme => style(theme, { ready, theme: props.theme }).item}>
              <Grid container alignItems='center'>
                <Grid item xs={10} sm={8}>
                  <Grid container alignItems='center' spacing={2}>
                    <Grid item>
                      <Avatar
                        css={theme =>
                          style(theme, { color, theme: props.theme }).avatar
                        }>
                        {name && name.slice(0, 1)}
                      </Avatar>
                    </Grid>
                    <Grid item xs={9} sm={5} md={6}>
                      <Grid container alignItems='center'>
                        <Typography
                          variant='body1'
                          css={theme => style(theme, { name }).name}>
                          {name ? name : key.slice(0, 10)}
                        </Typography>
                        {key === host && (
                          <HomeIcon
                            css={theme => style(theme, props).icon.host}
                          />
                        )}
                        {socket && key === socket.id && (
                          <FiberManualRecordIcon
                            css={theme => style(theme, props).icon.user}
                          />
                        )}
                      </Grid>
                    </Grid>

                    {(wins > 0 || losses > 0) && (
                      <Grid item xs={12} sm={5} md={4}>
                        {wins > 0 && (
                          <Chip
                            css={theme =>
                              style(theme, { isWin: true, ...props }).chip
                            }
                            variant='outlined'
                            label={wins}
                            // size='small'
                            icon={<StarIcon />}
                          />
                        )}
                        {losses > 0 && (
                          <Chip
                            css={theme =>
                              style(theme, { isWin: false, ...props }).chip
                            }
                            variant='outlined'
                            label={losses}
                            icon={<SentimentVeryDissatisfiedSharpIcon />}
                          />
                        )}
                      </Grid>
                    )}
                  </Grid>
                </Grid>
                <Grid
                  xs={2}
                  sm={4}
                  item
                  css={theme =>
                    style(theme, { ready, theme: props.theme }).ready
                  }>
                  <Grid
                    container
                    direction='row'
                    justify='flex-end'
                    alignItems='center'>
                    <Hidden xsDown>
                      <Typography variant='body1'>
                        {ready
                          ? text[props.language].join.ready
                          : text[props.language].join.unready}
                      </Typography>
                    </Hidden>
                    {ready ? <ThumbUpIcon /> : <ThumbDownIcon />}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          )
        )}
      <div ref={containerEndRef} />
    </Grid>
  )
}

const mapStateToProps = state => ({
  language: state.app.language,
  players: state.lobby.players,
  host: state.lobby.host,
  theme: state.app.theme,
  socket: state.socket
})

export default connect(mapStateToProps, {})(Players)
