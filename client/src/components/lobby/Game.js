/** @jsx jsx */
import React from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { connect } from 'react-redux'
import { Grid, Hidden } from '@material-ui/core'
import { text } from '../../content/language'
import { icons } from '../../content/icons'

const style = (theme, props) => ({
  gameContainer: {
    order: 2,
    '&': theme.typography.h5,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    color: theme.palette.primary[props.theme],
    fontSize: '1.618rem',
    paddingLeft: '1rem',
    '& svg': {
      width: 40,
      height: 40,
      padding: '0rem 1rem',
    },
    span: {
      padding: '0.25rem 0',
      fontWeight: 'bold',
    },
  },
  lobbyIdContainer: {
    fontFamily: theme.typography.fontFamily,
    fontSize: '3rem',
    fontWeight: 300,
    opacity: 0.2,
    padding: '0rem 2rem 0rem 1rem',
  },
})

const Game = (props) => {
  return (
    props.game && (
      <Grid item xs={11} sm={11} md={8} lg={6} xl={4}>
        <Grid container justify='space-between' alignItems='center'>
          <Grid item css={(theme) => style(theme, props).gameContainer}>
            <span>
              {text[props.language].games[props.game]
                ? text[props.language].games[props.game].name
                : props.game}
            </span>
            <Hidden smDown>{icons[props.game] || icons.unknown}</Hidden>
          </Grid>
          <Grid
            item
            xs={12}
            sm={6}
            css={(theme) => style(theme, props).lobbyIdContainer}>
            {props.lobbyId}
          </Grid>
        </Grid>
      </Grid>
    )
  )
}

const mapStateToProps = (state) => ({
  game: state.lobby.game,
  language: state.app.language,
  theme: state.app.theme,
  lobbyId: state.lobby.id,
  // game: 'Schnapsen',
})

export default connect(mapStateToProps)(Game)
