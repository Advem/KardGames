/** @jsx jsx */
import React, { useState } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux
import { connect } from 'react-redux'

// Components
import Subtitle from '../elements/Subtitle'
import ShortButton from '../elements/ShortButton'
import Animate from '../elements/Animate'
import InfoText from '../elements/InfoText'

// Style
import { Grid, ButtonBase } from '@material-ui/core'
import { text } from '../../content/language'
import { colors, s, alpha, global } from '../../assets/style'

// Icons
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'

const UserForm = (props) => {
  const [userName, setUserName] = useState('')
  const [chosenColor, setChosenColor] = useState(colors.text.subtitle)
  const [formError, setFormError] = useState(false)

  const handleSubmitDefault = (event) => {
    event.preventDefault()
    handleSubmit()
  }
  const handleChange = (event) => {
    setUserName(event.target.value.replace(/[^a-zA-Z0-9]/g, ''))
    if (event.target.value.length >= 3) setFormError(false)
  }

  const handleSubmit = () => {
    if (userName.length < 3) {
      setFormError(true)
    } else if (userName.length >= 3) {
      setFormError(false)
      props.dispatch({
        type: 'USER_SET_STATE',
        payload: { name: userName, color: chosenColor },
      })
    }
  }

  return (
    <Animate appear={props.appear} fadeEnter={1000} collEnter={500}>
      <div css={style.outerContainer}>
        <form onSubmit={handleSubmitDefault}>
          <Subtitle
            extraCSS={{
              [s.xs]: { marginBottom: '1rem' },
              [s.sm]: { marginBottom: '2rem' },
            }}>
            {text[props.language].user.subtitle}
          </Subtitle>
          <Grid container justify='center' alignItems='center'>
            <Grid
              item
              xs={12}
              sm={8}
              md={6}
              lg={5}
              xl={3}
              css={style.container}>
              <Grid container>
                <Grid item xs={12}>
                  <Grid
                    container
                    direction='row'
                    justify='space-evenly'
                    alignItems='center'
                    wrap='nowrap'
                    css={style.userContainer}>
                    <div
                      css={[
                        style.colorIndicator,
                        styleColor(chosenColor),
                      ]}></div>
                    <ButtonBase css={style.buttonBase}>
                      <input
                        type='text'
                        maxLength='16'
                        value={userName}
                        placeholder={text[props.language].user.placeholder}
                        onChange={handleChange}
                        css={[style.inputField, formError && style.formError]}
                      />
                    </ButtonBase>
                    <div onClick={() => handleSubmit()}>
                      <ShortButton
                        text='SAVE'
                        icon={<ArrowForwardIosIcon fontSize='small' />}
                      />
                    </div>
                  </Grid>
                </Grid>
                <Grid item xs={12}>
                  <Grid
                    container
                    direction='row'
                    justify='space-evenly'
                    alignItems='center'
                    css={style.colorsContainer}>
                    {colors.user.map((color, index) => (
                      <div
                        key={index}
                        onClick={() => setChosenColor(color)}
                        css={[
                          style.pickColor,
                          styleColor(color),
                          chosenColor === color && styleBorder(color),
                        ]}></div>
                    ))}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </form>
        <InfoText appear={formError} extraCSS={{ color: colors.error.text }}>
          <span>{text[props.language].user.error}</span>
        </InfoText>
      </div>
    </Animate>
  )
}

const style = {
  outerContainer: global.containerMargin,
  container: {
    backgroundColor: colors.background.dark,
    borderRadius: '2rem',
    [s.xs]: { margin: '0 1rem', padding: '1rem' },
    [s.ss]: {
      margin: '0 1rem',
      padding: '0.5rem',
      borderRadius: '1rem',
    },
    [s.sm]: { margin: '0 2rem', padding: '1rem' },
  },
  colorsContainer: {
    backgroundColor: colors.background.light,
    padding: '0.5rem 0.5rem',
    [s.xs]: { justifyContent: 'flex-start', borderRadius: '1rem' },
    [s.sm]: { justifyContent: 'space-evenly', borderRadius: '2rem' },
  },
  pickColor: {
    transition: 'all 0.15s ease-out',
    // width: '1.25rem',
    // height: '1.25rem',
    width: '32px', // ???
    height: '32px', // ???
    borderRadius: '2rem',
    margin: '0rem 0rem',
    border: `0.5rem solid ${colors.background.light}`,
    '&:hover': {
      cursor: 'pointer',
      borderColor: `${colors.background.light}${alpha[75]}`,
    },
  },
  userContainer: {
    [s.xs]: { padding: '0 0 1rem 0' },
    [s.ss]: { padding: '0 0 0.5rem 0' },
    [s.sm]: { padding: '0 0 1rem 0' },
  },
  colorIndicator: {
    // width: '1.25rem',
    // height: '1.25rem',
    width: '40px', // ???
    height: '40px', /// ???
    borderRadius: '2rem',
    border: `0.625rem solid ${colors.background.light}`,
    transition: 'background-color 0.15s ease-out',
  },
  inputField: {
    color: colors.text.primary,
    backgroundColor: colors.background.lighter,
    opacity: 0.5,
    width: '100%',
    fontSize: 'inherit',
    fontFamily: 'Spartan',
    fontWeight: 700,
    transition: 'all 0.3s ease-out',
    padding: '0.625rem 1rem',
    appearance: 'none',
    border: 'none',
    cursor: 'pointer',
    borderRadius: 'inherit',
    '&:hover': { opacity: 0.75 },
    '&:focus': { outline: 'none', opacity: 1 },
    '&::placeholder': {
      fontWeight: 400,
      letterSpacing: 0,
      color: 'inherit',
      opacity: 0.75,
    },
  },
  buttonBase: {
    borderRadius: '2rem',
    fontSize: '0.875rem',
    [s.xs]: { margin: '0 0.5rem 0 1rem', flexShrink: 100 },
    [s.ss]: { margin: '0 0.5rem 0 0.5rem' },
    [s.sm]: { margin: '0 1rem', flexGrow: 100 },
    '@media screen and (min-width: 408px)': { flexGrow: 100 },
  },
  formError: {
    backgroundColor: colors.error.background,
    color: colors.error.text,
  },
}

const styleColor = (color) => ({
  backgroundColor: color,
  color: color,
})

const styleBorder = (color) => ({
  borderColor: `${colors.background.light}${alpha[50]}`,
  '&:hover': {
    cursor: 'pointer',
    borderColor: `${colors.background.light}${alpha[50]}`,
  },
})

const mapStateToProps = (state) => ({
  language: state.app.language,
})

export default connect(mapStateToProps)(UserForm)
