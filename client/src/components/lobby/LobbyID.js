/** @jsx jsx */
import React, { useEffect, useState, useRef } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  _createLobby,
  _deleteLobby,
  _setLobbyHost,
  _setLobbyId,
} from '../../redux/actions'

// Components
import Animate from '../elements/Animate'
import InfoText from '../elements/InfoText'

// Styling
import { text } from '../../content/language'
import { s, colors, global } from '../../assets/style' // eslint-disable-line no-unused-vars
import { clientURL } from '../../utils/endpoints'

// Material UI
import { Grid, ButtonBase } from '@material-ui/core'

const LobbyID = ({
  appear,
  language,
  stage,
  lobbyId,
  lobbyGame,
  connected,
  _createLobby,
  _deleteLobby,
  _setLobbyHost,
  _setLobbyId,
}) => {
  const [linkCopied, setCopied] = useState(false)
  const refLink = useRef(null)

  // Update Host on reconnect
  useEffect(() => {
    if (connected) _setLobbyHost()
  }, [connected, _setLobbyHost])

  // Create Lobby on server
  useEffect(() => {
    if (connected && lobbyId) _createLobby()
  }, [connected, lobbyId, _createLobby])

  // Delete lobby from server
  useEffect(() => {
    return () => lobbyId && stage !== 'game' && _deleteLobby(lobbyId)
  }, [lobbyId, _deleteLobby, stage])

  useEffect(() => {
    const timer = setTimeout(() => {
      if (linkCopied) setCopied(false)
    }, 3000)
    return () => clearTimeout(timer)
  }, [linkCopied, setCopied])

  const handleLinkClick = () => {
    const link = `${clientURL}/join/${refLink.current.innerHTML}`
    if (navigator && navigator.clipboard)
      navigator.clipboard.writeText(link).then(
        () => {
          if (!linkCopied) setCopied(true)
        },
        () => {
          setCopied('error')
        }
      )
    else setCopied('error')
  }

  return (
    <Animate appear={appear}>
      <Grid
        container
        direction='column'
        alignItems='center'
        onClick={() => handleLinkClick()}
        css={style.container}>
        <ButtonBase css={[style.buttonBase, styleButtonColor(linkCopied)]}>
          <span ref={refLink}>
            {lobbyId || text[language].join.form.placeholder}
          </span>
        </ButtonBase>
        <InfoText extraCSS={styleInfoText(linkCopied)} appear={Boolean(true)}>
          {text[language].create.linkCopied[linkCopied.toString()]}
        </InfoText>
      </Grid>
    </Animate>
  )
}

const styleInfoText = (active) => ({
  color: active
    ? active === 'error'
      ? colors.error.text
      : colors.info.text
    : colors.text.secondary,
  svg: { width: 16 },
  cursor: 'pointer',
  opacity: active ? 1 : 0.5,
  '&:hover': { opacity: 1 },
  [s.xs]: {
    color: !active && colors.text.secondary,
    opacity: 1,
  },
})

const styleButtonColor = (active) => {
  let state = active ? (active === 'error' ? active : 'info') : 'default'

  return {
    '&:hover': { opacity: 1 },
    opacity: active ? 1 : 0.5,
    backgroundColor: colors[state].background,
    color: colors[state].text,
  }
}

const style = {
  container: [global.containerMargin],
  buttonBase: [
    {
      width: '100%',
      cursor: 'pointer',
      borderRadius: 'inherit',
      textTransform: 'uppercase',
      fontFamily: 'Spartan',
      fontWeight: 400,
      letterSpacing: 3,
      transition: 'all 0.3s ease-out',
      padding: '0.75rem 1rem',
      textAlign: 'center',
      appearance: 'none',
      border: 'none',
    },
    global.lobbyIdButtonBase,
  ],
  wrapperDiv: {
    width: '100%',
  },
}

const mapStateToProps = (state) => ({
  language: state.app.language,
  stage: state.app.stage,
  lobbyId: state.lobby.id,
  lobbyGame: state.lobby.game,
  connected: state.app.connected,
})

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { _createLobby, _deleteLobby, _setLobbyHost, _setLobbyId, dispatch },
    dispatch
  )
}
export default connect(mapStateToProps, mapDispatchToProps)(LobbyID)
