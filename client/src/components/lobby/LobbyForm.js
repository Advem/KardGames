/** @jsx jsx */
import React, { useEffect, useState } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux
import { connect } from 'react-redux'
import { _findLobby, _setLobbyId } from '../../redux/actions'

// Components
import Animate from '../elements/Animate'

// Style
import { Grid, ButtonBase } from '@material-ui/core'
import { text } from '../../content/language'
import { colors, global } from '../../assets/style'

// Icons
import DoneIcon from '@material-ui/icons/Done'
import BlockIcon from '@material-ui/icons/Block'
import CircularProgress from '@material-ui/core/CircularProgress'
import InfoText from '../elements/InfoText'

const LobbyForm = ({
  connected,
  lobby,
  language,
  initial,
  _findLobby,
  _setLobbyId,
}) => {
  const [inputValue, setInputValue] = useState('')
  const [status, setStatus] = useState('default')

  useEffect(() => {
    if (initial && initial.length <= 6) {
      setInputValue(initial)
      _findLobby(initial)
    }
  }, [connected, initial, _findLobby, setInputValue])

  useEffect(() => {
    if (lobby === undefined) setStatus('warning')
    else if (lobby) return setStatus('success')
    else if (lobby === null) setStatus('default')
    else if (lobby === false) setStatus('error')
  }, [setStatus, lobby])

  const handleChange = (event) => {
    setInputValue(event.target.value.toUpperCase().replace(/[^a-zA-Z0-9]/g, ''))
    if (event.target.value.length < 6) _setLobbyId(null)
    if (event.target.value.toUpperCase().length === 6) {
      _findLobby(event.target.value.toUpperCase())
    }
  }

  const handleSubmit = (event) => event.preventDefault()

  return (
    <Animate appear={true}>
      <Grid
        container
        justify='center'
        direction='column'
        alignItems='center'
        css={[style.container, colorizeFront(status)]}>
        {/* <Grid item xs={12}> */}
        <form autoComplete='off' onSubmit={handleSubmit}>
          <ButtonBase
            disableRipple={lobby || lobby === undefined ? true : false}
            css={style.buttonBase}>
            <input
              type='text'
              maxLength='6'
              value={inputValue}
              placeholder={text[language].join.form.placeholder}
              disabled={lobby || lobby === undefined}
              onChange={handleChange}
              css={[style.inputField, colorizeBack(status)]}
            />
          </ButtonBase>
        </form>
        <InfoText appear={Boolean(lobby !== null)}>
          {renderIcon(lobby)}
          <span>{text[language].join.form.response[status]}</span>
        </InfoText>
        {/* </Grid> */}
      </Grid>
    </Animate>
  )
}

const colorizeFront = (status) => ({
  color: colors[status].text,
})

const colorizeBack = (status) => ({
  backgroundColor: colors[status].background,
})

const renderIcon = (status) => {
  if (status) return <DoneIcon />
  else if (status === undefined)
    return <CircularProgress size='1.25rem' color='inherit' />
  else if (status === false) return <BlockIcon />
}

const style = {
  container: global.containerMargin,
  inputField: {
    color: 'inherit',
    opacity: 0.5,
    width: '100%',
    fontSize: 'inherit',
    fontFamily: 'Spartan',
    fontWeight: 400,
    letterSpacing: 3,
    // transition: 'background-color 3s ease-out',

    transition: 'all 0.3s ease-out',
    transitionProperty: 'opacity, background-color',
    padding: '0.75rem 1rem',
    textAlign: 'center',
    appearance: 'none',
    border: 'none',
    cursor: 'pointer',
    borderRadius: 'inherit',
    textTransform: 'uppercase',
    // [s.xs]: {
    //   opacity: 0.75,
    // },
    '&:hover': { opacity: 0.75 },
    '&:focus': { outline: 'none', opacity: 1 },
    '&::placeholder': {
      fontWeight: 400,
      letterSpacing: 0,
      color: 'inherit',
      opacity: 0.75,
    },
  },
  buttonBase: [
    {
      transition: 'color 0.3s ease-out',
    },
    global.lobbyIdButtonBase,
  ],
}

const mapStateToProps = (state) => ({
  language: state.app.language,
  lobby: state.lobby.id,
  connected: state.app.connected,
})

export default connect(mapStateToProps, {
  _findLobby,
  _setLobbyId,
})(LobbyForm)
