/** @jsx jsx */
import React, { useEffect } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars

// Redux
import { connect } from 'react-redux'

// Components
import Animate from '../elements/Animate'

// Styling
import { colors } from '../../assets/style'

// Material UI
import { Grid } from '@material-ui/core'

const Main = () => {
  useEffect(() => {
    return () => console.log('SHOULD DELETE PLAYER')
  }, [])
  return (
    <Grid container>
      <Grid item css={testStyle}>
        LOBBY
      </Grid>
      <Grid item css={testStyle}>
        CHAT
      </Grid>
    </Grid>
  )
}

const LobbyMain = ({ appear, ...props }) => (
  <Animate appear={appear}>
    <Main />
  </Animate>
)

const testStyle = {
  width: '100%',
  height: '100%',
  backgroundColor: colors.background.light,
}

const mapStateToProps = (state) => ({
  lobbyId: state.lobby.id,
  lobbyGame: state.lobby.game,
  lobbyHost: state.lobby.host,
  lobbyOpen: state.lobby.open,
  players: state.lobby.players,
})

export default connect(mapStateToProps)(LobbyMain)
