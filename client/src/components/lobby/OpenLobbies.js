/** @jsx jsx */
import React, { useEffect, useState } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { Link } from 'react-router-dom'
import _ from 'lodash'

// Content
import { colors as gameColors, names as gameNames } from '../../content/games'

// Redux
import { connect } from 'react-redux'

// Components
import Title2 from '../elements/Title2'
import ShortButton from '../elements/ShortButton'
import Animate from '../elements/Animate'
import SortMenu from '../lobby/SortMenu'

// Style
import { Grid } from '@material-ui/core'
import { s, colors, alpha } from '../../assets/style'
import { text } from '../../content/language'

// Icons
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import CircularProgress from '@material-ui/core/CircularProgress'
import PeopleIcon from '@material-ui/icons/People'

const OpenLobbies = ({ lobbies, language, appear }) => {
  const [selectedLobby, setSelectedLobby] = useState(null)
  const [sortMethod, setSortMethod] = useState('newestASC')

  const sortLobbies = () => {
    let sortedLobbies

    switch (sortMethod) {
      case 'playersASC':
        sortedLobbies = _.sortBy(lobbies, [
          (a) => Object.keys(a.players).length,
        ])
        break
      case 'playersDESC':
        sortedLobbies = _.sortBy(lobbies, [
          (a) => Object.keys(a.players).length,
        ]).reverse()
        break
      case 'gamesASC':
        sortedLobbies = _.sortBy(lobbies, ['game']).reverse()
        break
      case 'gamesDESC':
        sortedLobbies = _.sortBy(lobbies, ['game'])
        break
      case 'hostsASC':
        sortedLobbies = _.sortBy(lobbies, [(a) => a.players[a.host]]).reverse()
        break
      case 'hostsDESC':
        sortedLobbies = _.sortBy(lobbies, [(a) => a.players[a.host]])
        break
      case 'newestASC':
        sortedLobbies = _.values(lobbies)
        break
      case 'newestDESC':
        sortedLobbies = _.values(lobbies).reverse()
        break
      default:
        sortedLobbies = _.values(lobbies)
    }

    return sortedLobbies.map(({ id, host, game, players }) => (
      <Grid
        container
        alignItems='center'
        justify='space-between'
        key={id}
        css={style.openLobby}
        onClick={() =>
          selectedLobby === id ? setSelectedLobby(null) : setSelectedLobby(id)
        }>
        <div css={style.leftPart}>
          <div
            css={[
              style.players,
              { backgroundColor: gameColors[game] || gameColors.unknown },
            ]}>
            {Object.keys(players).length}
          </div>
          <span
            css={[
              style.game,
              style.truncate,
              { color: gameColors[game] || colors.text.secondary },
            ]}>
            {gameNames[game]
              ? gameNames[game][language]
              : gameNames.unknown[language]}
          </span>
          <span css={[style.host, style.truncate]}>
            {(players[host] && players[host].name) || host}
          </span>
          <span css={style.id}>{id}</span>
        </div>
        <Link to={`/join/${id}`}>
          <ShortButton
            text={text[language].openLobbies.join}
            background={colors.background.light}
            icon={<ChevronRightIcon />}
          />
        </Link>
        <Animate
          fadeEnter={1000}
          collEnter={600}
          extraCSS={{ minWidth: '100%' }}
          appear={selectedLobby === id}>
          <div css={style.lobbyPlayers}>
            <div css={style.lobbyPlayers.icon}>
              <PeopleIcon />
              <span css={{ textTransform: 'capitalize' }}>
                {text[language].players}:
              </span>
            </div>
            {Object.keys(players).map((player) => (
              <span key={player}>{players[player].name || player}</span>
            ))}
          </div>
        </Animate>
      </Grid>
    ))
  }

  const renderLobbies = () =>
    lobbies ? sortLobbies() : <CircularProgress size='2rem' color='inherit' />

  return (
    <Animate appear={appear}>
      <div>
        <Grid container justify='center' alignItems='center'>
          <Grid item xs={12} sm={10} md={8} lg={6} xl={4} css={style.header}>
            <Grid container justify='space-between' alignItems='center'>
              <Grid item>
                <Title2>{text[language].openLobbies.title}</Title2>
              </Grid>
              <Grid item>
                {/* {renderSortMenu()} */}
                <SortMenu
                  sortMethod={sortMethod}
                  setSortMethod={setSortMethod}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container justify='center' alignItems='center'>
          <Grid
            item
            xs={12}
            sm={10}
            md={8}
            lg={6}
            xl={4}
            css={style.openLobbies}>
            {renderLobbies()}
          </Grid>
        </Grid>
      </div>
    </Animate>
  )
}

const style = {
  header: {
    [s.xs]: { padding: '0 1rem' },
    [s.sm]: { padding: '0' },
    marginBottom: '0.75rem',
  },
  openLobbies: {
    [s.xs]: { padding: '0 1rem', marginBottom: '2rem' },
    [s.sm]: { padding: '0', marginBottom: '3rem' },
    [s.xl]: { padding: '0', marginBottom: '4rem' },
  },
  openLobby: {
    minHeight: '3rem',
    marginBottom: '0.75rem',
    padding: '0.5rem 0.5rem',
    borderRadius: '1.5rem',
    backgroundColor: colors.background.dark,
    '& span': {
      paddingLeft: '0.5rem',
    },
    '&:hover': {
      cursor: 'pointer',
    },
  },
  leftPart: {
    display: 'flex',
    alignItems: 'center',
  },
  lobbyPlayers: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    minWidth: '100%',
    '& span': {
      padding: '0.25rem 0.25rem',
      color: colors.text.secondary.concat(alpha[50]),
      fontWeight: 400,
      fontSize: '0.75rem',
      [s.xs]: {
        fontSize: '0.625rem',
      },
    },
    icon: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: '1rem',
      padding: '0.25rem 0',
      color: colors.text.secondary.concat(alpha[100]),
      marginLeft: '0.25rem',
      fontSize: '0.75rem',
      [s.xs]: {
        marginLeft: '0.5rem',
        fontSize: '0.625rem',
      },
      '& span': {
        fontSize: 'inherit',
        // marginLeft: '0.25rem',
        padding: '0 0.5rem',
        fontWeight: 'bold',
        color: 'inherit',
      },
      '& svg': {
        [s.xs]: {
          fontSize: '1rem',
        },
        fontSize: '1.25rem',
      },
    },
  },
  players: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '2rem',
    height: '2rem',
    fontSize: '1.125rem',
    fontWeight: 'bold',
    color: 'white',
    borderRadius: '1rem',
  },
  game: {
    fontWeight: 'bold',
    fontSize: '1rem',
    letterSpacing: '-1px',
    [s.xs]: { fontSize: '1rem' },
    [s.sss]: { fontSize: '0.875rem' },
  },
  host: {
    color: colors.text.secondary.concat(alpha[75]),
    fontSize: '0.875rem',
  },
  id: {
    [s.xs]: { display: 'none' },
    color: colors.background.lighter,
    fontWeight: 300,
  },
  truncate: {
    [s.xs]: {
      maxWidth: 100,
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
    [s.sss]: {
      maxWidth: 72,
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'clip',
    },
  },
}

const mapStateToProps = (state) => ({
  language: state.app.language,
  lobbies: state.lobbies,
  connected: state.app.connected,
})

export default connect(mapStateToProps)(OpenLobbies)
