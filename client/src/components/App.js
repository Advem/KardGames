/** @jsx jsx */
import React, { useEffect } from 'react' // eslint-disable-line no-unused-vars
import { jsx, css } from '@emotion/core' // eslint-disable-line no-unused-vars
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

// Redux
import { connect } from 'react-redux'
import { _connectServer, _eventsListener } from '../redux/actions'

// Components
import Home from './views/Home'
import CreateGame from './views/CreateGame'
import JoinGame from './views/JoinGame'
import Header from './elements/Header'
import Connection from './elements/Connection'

// Styles
import '../assets/style/index.css'
import { colors, alpha, s } from '../assets/style'

const App = ({ _connectServer, _eventsListener, socket, ...props }) => {
  useEffect(() => {
    _connectServer()
  }, [_connectServer])

  useEffect(() => {
    if (socket) _eventsListener()
  }, [socket, _eventsListener])

  return (
    <div css={style.app}>
      <Router>
        <Header />
        <Switch>
          <Route exact path='/'>
            <Home />
          </Route>
          <Route path='/create'>
            <CreateGame />
          </Route>
          <Route exact path='/join'>
            <JoinGame />
          </Route>
          <Route path='/join/:lobby'>
            <JoinGame />
          </Route>
        </Switch>
      </Router>
      <Connection />
    </div>
  )
}

const style = {
  app: {
    width: '100vw',
    minHeight: '100vh',
    backgroundColor: colors.background.darker.concat(alpha[100]),
    color: colors.text.primary,
    paddingTop: 64, // fixed header
    [s.xs]: { marginTop: '2rem' }, // fixed header
    [s.sm]: { marginTop: '4rem' }, // fixed header
  },
}

const mapStateToProps = (state) => ({
  stage: state.app.stage,
  socket: state.socket,
})

export default connect(mapStateToProps, { _connectServer, _eventsListener })(
  App
)
