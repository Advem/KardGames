import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import reduxThunk from 'redux-thunk'
import reducers from './redux/reducers/'
import App from './components/App'

import { theme } from './assets/theme'
import { ThemeProvider } from 'emotion-theming'
import {
  ThemeProvider as MuiThemeProvider,
  StylesProvider,
} from '@material-ui/core/styles'
import { CssBaseline } from '@material-ui/core'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(reduxThunk))
)

console.log(theme)

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <ThemeProvider theme={theme}>
        <StylesProvider injectFirst>
          <CssBaseline />
          <App />
        </StylesProvider>
      </ThemeProvider>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root')
)
